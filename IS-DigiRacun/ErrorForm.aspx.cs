﻿using System;
using System.Web.UI.WebControls;

namespace IS_DigiRacun
{
    public partial class ErrorForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["user"] != null)
                {
                    L_user.Text = Session["user"].ToString();

                    M_mainMenu.Items.Clear();
                    M_mainMenu.Items.Add(new MenuItem("Odjava", "logout"));
                    M_mainMenu.Items.Add(new MenuItem("Uporabniški profil", "userProfile"));
                    M_mainMenu.Items.Add(new MenuItem("Domov", "home"));
                }
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "login":
                    Response.Redirect("Login.aspx");
                    break;
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Response.Redirect("ProfileForm.aspx");
                    break;
                case "home":
                    redirect();
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void B_home_Click(object sender, EventArgs e)
        {
            redirect();
        }
    }
}