﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditReceipt.aspx.cs" Inherits="IS_DigiRacun.EditReceipt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Podrobnosti računa - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_itemsReceipt" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT rp.id, p.productName, rp.priceUponPurchase, rp.quantity, rp.productSerialNo, rp.dateOfGuarantee, rp.quantity * rp.priceUponPurchase AS 'sumPrice' FROM [Receipt] r
INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id
INNER JOIN [Product] p ON p.id = rp.idProduct
WHERE r.id = @receiptId
ORDER BY p.productName ASC">
                <SelectParameters>
                    <asp:SessionParameter Name="receiptId" SessionField="receiptId" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_productMaterials" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT DISTINCT p.id AS 'productId', p.productName, m.id AS 'materialId', m.material, m.fileName, m.description, m.picture FROM [Receipt] r
INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id
INNER JOIN [Product] p ON p.id = rp.idProduct
INNER JOIN [Materials] m ON m.idProduct = p.id
WHERE r.id = @receiptId AND m.active = 1
ORDER BY p.productName ASC">
                <SelectParameters>
                    <asp:SessionParameter Name="receiptId" SessionField="receiptId" />
                </SelectParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Pregled izdelkov za nakup" Value="shopWindow"></asp:MenuItem>
                            <asp:MenuItem Text="Pregled gradiv izdelkov na računih" Value="userMaterials"></asp:MenuItem>
                            <asp:MenuItem Text="Nazaj na upravljanje računov" Value="userForm"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">Podrobnosti računa</p>
                    <p class="contentH2">Uporabnik</p>
                </div>
                <div class="content">
                    <asp:Panel ID="P_editReceipt" runat="server" DefaultButton="B_save">
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>
                                    Trgovina:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_shop" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_shop" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_editReceipt"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    Datum nakupa:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_dateOfPurchase" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_dateOfPurchase" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_editReceipt"></asp:RequiredFieldValidator>
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_shop" runat="server" ValidationGroup="VG_editReceipt"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_dateOfPurchase" runat="server" TextMode="Date" ValidationGroup="VG_editReceipt"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>
                                    Št. računa:
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:Label ID="L_dateOfGuarantee" runat="server" Text="Garancija do:"></asp:Label>
                                    <br />
                                    <br />
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_receiptNumber" runat="server" ValidationGroup="VG_editReceipt"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_dateOfGuarantee" runat="server" TextMode="Date" ValidationGroup="VG_editReceipt"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                    <div class="contentCentral">
                        <asp:Image ID="IMG_receiptPicture" runat="server" />
                        <br />
                        <asp:Button ID="B_downloadPicture" runat="server" Text="Prenesi" OnClick="B_downloadPicture_Click" />
                    </div>
                    <asp:GridView ID="GV_products" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SDS_itemsReceipt" Width="961px" OnRowDataBound="GV_products_RowDataBound" ShowFooter="True">
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                            <asp:BoundField DataField="productName" HeaderText="Artikel" SortExpression="productName" FooterText="Skupaj:">
                                <FooterStyle Font-Bold="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="priceUponPurchase" HeaderText="Cena" SortExpression="priceUponPurchase" DataFormatString="{0:c}" />
                            <asp:BoundField DataField="quantity" HeaderText="Količina" SortExpression="quantity" />
                            <asp:BoundField DataField="productSerialNo" HeaderText="Serijska št." SortExpression="productSerialNo" />
                            <asp:BoundField DataField="dateOfGuarantee" HeaderText="Garancija do" SortExpression="dateOfGuarantee" DataFormatString="{0:dd. MM. yyyy}" />
                            <asp:BoundField DataField="sumPrice" HeaderText="Cena skupaj" ReadOnly="True" SortExpression="sumPrice" DataFormatString="{0:c}" />
                        </Columns>
                    </asp:GridView>
                    <br />
                    <asp:GridView ID="GV_materials" runat="server" AutoGenerateColumns="False" DataSourceID="SDS_productMaterials" Width="961px" AllowPaging="True" AllowSorting="True" OnRowDataBound="GV_materials_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="productId" HeaderText="productId" SortExpression="productId" Visible="False" />
                            <asp:BoundField DataField="productName" HeaderText="Artikel" SortExpression="productName" />
                            <asp:BoundField DataField="materialId" HeaderText="materialId" SortExpression="materialId" Visible="False" />
                            <asp:BoundField DataField="material" HeaderText="material" SortExpression="material" Visible="False" />
                            <asp:TemplateField HeaderText="Gradivo">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnMaterial" runat="server" Value='<%# Bind("material") %>' />
                                    <asp:HiddenField ID="hdnPicture" runat="server" Value='<%# Bind("picture") %>' />
                                    <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Bind("fileName") %>' />
                                    <asp:Image ID="IMG_material" runat="server" />
                                    <br />
                                    <asp:Button ID="B_downloadFile" runat="server" OnClick="B_downloadFile_Click" Text="Prenesi" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="fileName" HeaderText="fileName" SortExpression="fileName" Visible="False" />
                            <asp:BoundField DataField="description" HeaderText="Opis" SortExpression="description" />
                            <asp:CheckBoxField DataField="picture" HeaderText="picture" SortExpression="picture" Visible="False" />
                        </Columns>
                    </asp:GridView>
                    <div class="content">
                        <div class="contentHalf" style="width: 480px">
                            <p style="text-align: left; margin-left: 40px;">
                                <asp:Button ID="B_deleteReceipt" runat="server" Text="Izbriši račun" OnClick="B_deleteReceipt_Click" OnClientClick="return confirm('Ali ste prepričani, da želite izbrisati račun?')" />
                            </p>
                        </div>
                        <div class="contentHalf" style="width: 480px">
                            <p style="text-align: right; margin-right: 40px;">
                                <asp:Button ID="B_save" runat="server" Text="Shrani spremembe" OnClick="B_save_Click" ValidationGroup="VG_editReceipt" />
                                &nbsp;
                                <asp:Button ID="B_cancel" runat="server" Text="Prekliči" OnClick="B_cancel_Click" />
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
