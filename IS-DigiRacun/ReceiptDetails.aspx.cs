﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_DigiRacun
{
    public partial class ReceiptDetails : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);
        private double priceTotal = 0.0;

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access ReceiptDetails.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false ||
                !(userAccess.Equals("B1B443E8-0D7B-4AD3-86E7-9688156A067D") || userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C")))
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                if (userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C"))
                {
                    M_mainMenu.Items.AddAt(3, new MenuItem("Pregled izdelkov v ponudbi", "sellerAdminForm"));
                }

                SqlCommand receiptData = new SqlCommand("SELECT shop, seller, receiptNumber, dateOfPurchase, code FROM [Receipt] WHERE id = @receiptId", connection);
                receiptData.Parameters.AddWithValue("@receiptId", Session["receiptId"].ToString());
                connection.Open();
                SqlDataReader reader = receiptData.ExecuteReader();

                if (reader.Read())
                {
                    TB_shopName.Text = reader.GetString(0);
                    TB_seller.Text = reader.GetString(1);
                    TB_receiptNumber.Text = reader.GetString(2);
                    TB_dateOfPurchase.Text = reader.GetDateTime(3).ToString("yyyy-MM-dd");
                    IMG_qrCode.ImageUrl = String.Format("http://chart.apis.google.com/chart?cht=qr&chs=100x100&chl={0}", reader.GetString(4));
                    L_receiptCode.Text = reader.GetString(4);
                }

                reader.Close();
                connection.Close();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "sellerForm":
                    Session["receiptId"] = null;
                    Server.Transfer("SellerForm.aspx");
                    break;
                case "sellerAdminForm":
                    Server.Transfer("SellerAdminForm.aspx");
                    break;
            }
        }

        protected void GV_products_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // check row type
            if (e.Row.RowType == DataControlRowType.DataRow)
                // if row type is DataRow, add sumPrice value to totalPrice
                priceTotal += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "sumPrice"));
            else if (e.Row.RowType == DataControlRowType.Footer)
                // if row type is footer, show calculated total value
                e.Row.Cells[6].Text = String.Format("{0:c}", priceTotal);
        }

        protected void B_deleteReceipt_Click(object sender, EventArgs e)
        {
            SqlCommand deleteReceipt = new SqlCommand("UPDATE [Receipt] SET activeShop = 0 WHERE id = @id", connection);
            deleteReceipt.Parameters.AddWithValue("@id", Session["receiptId"].ToString());

            connection.Open();
            deleteReceipt.ExecuteNonQuery();
            connection.Close();

            Server.Transfer("SellerForm.aspx");
        }

        protected void B_return_Click(object sender, EventArgs e)
        {
            Server.Transfer("SellerForm.aspx");
        }
    }
}