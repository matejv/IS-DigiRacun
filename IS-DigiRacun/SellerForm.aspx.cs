﻿using System;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class SellerForm : System.Web.UI.Page
    {
        private static readonly String getAllShopReceipts_SA = "SELECT * FROM [Receipt] WHERE [seller] IN (SELECT [username] FROM [User] WHERE [idShop] = (SELECT [idShop] FROM [User] WHERE [username] = @seller)) AND [activeShop] = 1 ORDER BY [dateOfPurchase] DESC, [receiptNumber] DESC";
        string userAccess;

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access SellerForm.aspx is redirected to Login.aspx
            userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false ||
                !(userAccess.Equals("B1B443E8-0D7B-4AD3-86E7-9688156A067D") || userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C")))
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                if (userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C"))
                {
                    M_mainMenu.Items.AddAt(3, new MenuItem("Pregled izdelkov v ponudbi", "sellerAdminForm"));
                }
            }
        }

        protected void Page_PreRender(object o, EventArgs e)
        {
            // Seller Administrator can see all receipts which were created in belonging shop
            if (userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C"))
            {
                SDS_receipt.SelectCommand = getAllShopReceipts_SA;
                GV_receipts.DataBind();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "newReceipt":
                    Session["idShop"] = getShopId(Session["user"].ToString());
                    Server.Transfer("NewReceipt.aspx");
                    break;
                case "sellerAdminForm":
                    Server.Transfer("SellerAdminForm.aspx");
                    break;
            }
        }

        protected void B_newReceipt_Click(object sender, EventArgs e)
        {
            Session["idShop"] = getShopId(Session["user"].ToString());

            Server.Transfer("NewReceipt.aspx");
        }

        protected void B_receiptDetails_Click(object sender, EventArgs e)
        {
            Button btnDetails = (Button)sender;
            GridViewRow gvr = (GridViewRow)btnDetails.NamingContainer;
            Session["receiptId"] = GV_receipts.DataKeys[gvr.RowIndex].Value.ToString();

            Server.Transfer("ReceiptDetails.aspx");
        }
    }
}