﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Security;

namespace IS_DigiRacun
{
    public static class BaseClass
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);

        // USER
        // gets a password extract using the MD5 function
        internal static string passwordExtract(string password)
        {
            byte[] hash = Encoding.UTF8.GetBytes(password);
            MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
            hash = md5hasher.ComputeHash(hash);
            StringBuilder extract = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
                extract.Append(hash[i]);

            return extract.ToString();
        }

        // checks if username already exists in database
        internal static bool usernameFree(string username)
        {
            SqlCommand checkUser = new SqlCommand("SELECT COUNT(*) FROM [User] WHERE username = @username", connection);
            checkUser.Parameters.AddWithValue("@username", username);

            connection.Open();
            int numberUsers = (int)checkUser.ExecuteScalar();
            connection.Close();

            return (numberUsers == 0) ? true : false;
        }

        // performs user registration and verifies if it was successful
        internal static bool userRegistration(string username, string password, string email, string name, string surname)
        {
            try
            {
                if (usernameFree(username))
                {
                    SqlCommand addUser = new SqlCommand("INSERT INTO [User](username, password, email, name, surname, access, active, idShop) VALUES(@username, @password, @email, @name, @surname, 'B75E7D25-0296-4AD0-833E-DB234C96151E', 1, '6EE88A65-CC22-49CB-983A-8E5629CE76BE')", connection);
                    addUser.Parameters.AddWithValue("@username", username);
                    addUser.Parameters.AddWithValue("@password", passwordExtract(password));
                    addUser.Parameters.AddWithValue("@email", email);

                    if (name != null)
                    {
                        addUser.Parameters.AddWithValue("@name", name);
                    }
                    else
                    {
                        addUser.Parameters.AddWithValue("@name", DBNull.Value);
                    }

                    if (surname != null)
                    {
                        addUser.Parameters.AddWithValue("@surname", surname);
                    }
                    else
                    {
                        addUser.Parameters.AddWithValue("@surname", DBNull.Value);
                    }

                    connection.Open();
                    addUser.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

            return false;
        }

        internal static bool checkLogin(string username, string password)
        {
            SqlCommand login = new SqlCommand("SELECT COUNT(*) FROM [User] WHERE username = @username COLLATE SQL_Latin1_General_CP1_CS_AS AND password = @password AND active = 1", connection);
            login.Parameters.AddWithValue("@username", username);
            login.Parameters.AddWithValue("@password", passwordExtract(password));

            connection.Open();
            int numberUsers = (int)login.ExecuteScalar();
            connection.Close();

            return (numberUsers == 1) ? true : false;
        }

        // gets user's email
        internal static string getEmail(string username)
        {
            SqlCommand getEmail = new SqlCommand("SELECT email FROM [User] WHERE username = @username", connection);
            getEmail.Parameters.AddWithValue("@username", username);

            connection.Open();
            string email = (getEmail.ExecuteScalar() == null) ? string.Empty : getEmail.ExecuteScalar().ToString();
            connection.Close();

            return email;
        }

        internal static string generateNewPassword()
        {
            string newPassword = Membership.GeneratePassword(8, 1);
            newPassword.Replace(':', '-');

            return newPassword;
        }

        // reset user's password
        internal static bool sendNewPassword(bool resettedPassword, string username, string newPassword)
        {
            string mailBodyResetPassword = string.Format(@"<p>Pozdravljeni, {0}!<br />
                                            Vaše geslo v uporabniškem računu za digitalno shranjevanje računov je bilo ponastavljeno.<br />
                                            Novo geslo: {1}<br />
                                            Lep dan vam želimo,<br />
                                            Ekipa Digi Račun</p><br />",
                                            username, newPassword);

            string mailBodyNewUser = string.Format(@"<p>Pozdravljeni, {0}!<br />
                                            Za vas je bil ustvarjen uporabniški račun v storitvi za digitalno shranjevanje računov.<br />
                                            Vaše uporabniško ime: {0}<br />
                                            Vaše geslo: {1}<br />
                                            Lep dan vam želimo,<br />
                                            Ekipa Digi Račun</p><br />",
                                            username, newPassword);

            bool successfullySent;

            if (resettedPassword)
            {
                successfullySent = sendEmail(getEmail(username), "Ponastavitev gesla", mailBodyResetPassword);
            }
            else
            {
                successfullySent = sendEmail(getEmail(username), "Nov uporabniški račun", mailBodyNewUser);
            }

            // update user's password in database
            if (successfullySent && resettedPassword)
            {
                SqlCommand changePassword = new SqlCommand("UPDATE [User] SET password = @newPassword WHERE username = @username", connection);
                changePassword.Parameters.AddWithValue("@newPassword", passwordExtract(newPassword));
                changePassword.Parameters.AddWithValue("@username", username);

                connection.Open();
                changePassword.ExecuteNonQuery();
                connection.Close();
            }

            return successfullySent;
        }

        internal static bool sendEmail(string addressTo, string subject, string body)
        {
            MailMessage msg = new MailMessage();
            msg.To.Add(new MailAddress(addressTo));
            msg.From = new MailAddress("digiracun@gmail.com", "Digi Račun - Ne odgovarjaj");
            msg.IsBodyHtml = true;
            msg.Subject = subject;
            msg.Body = body;

            // creates email client and sends email message
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("digiracun@gmail.com", "2rac*GID018dipl");
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;

            try
            {
                client.Send(msg);
            }
            catch (SmtpException)
            {
                return false;
            }

            return true;
        }

        // gets type of user
        internal static string getUserType(string username)
        {
            SqlCommand getUserType = new SqlCommand("SELECT access FROM [User] WHERE username = @username", connection);
            getUserType.Parameters.AddWithValue("@username", username);

            connection.Open();
            string access = getUserType.ExecuteScalar().ToString();
            connection.Close();

            return access;
        }

        // gets id of shop where seller works
        internal static string getShopId(string username)
        {
            SqlCommand getShopId = new SqlCommand("SELECT idShop FROM [User] WHERE username = @username", connection);
            getShopId.Parameters.AddWithValue("@username", username);

            connection.Open();
            string idShop = getShopId.ExecuteScalar().ToString();
            connection.Close();

            return idShop;
        }

        // gets shop name
        internal static string getShopName(string id)
        {
            SqlCommand getShopName = new SqlCommand("SELECT name FROM [Shop] WHERE id = @id", connection);
            getShopName.Parameters.AddWithValue("@id", id);

            connection.Open();
            string shopName = getShopName.ExecuteScalar().ToString();
            connection.Close();

            return shopName;
        }

        // gets new ID
        internal static string getNewId()
        {
            SqlCommand getNewID = new SqlCommand("SELECT TOP 1 NEWID()", connection);
            connection.Open();
            string newID = getNewID.ExecuteScalar().ToString();
            connection.Close();

            return newID;
        }

        internal static byte[] resizeImage(byte[] originalImg, int maxWidth, int maxHeight)
        {
            Bitmap originalImage;
            using (var ms = new MemoryStream(originalImg))
            {
                originalImage = new Bitmap(ms);
            }

            ImageFormat originalFormat = originalImage.RawFormat;
            Bitmap resizedImage = null;

            try
            {
                decimal newRatio;
                int newWidth = 0;
                int newHeight = 0;

                if (originalImage.Width < maxWidth && originalImage.Height < maxHeight)
                    return originalImg;

                if (originalImage.Width > originalImage.Height)
                {
                    newRatio = (decimal)maxWidth / originalImage.Width;
                    newWidth = maxWidth;
                    decimal tmpHeight = originalImage.Height * newRatio;
                    newHeight = (int)tmpHeight;
                }
                else
                {
                    newRatio = (decimal)maxHeight / originalImage.Height;
                    newHeight = maxHeight;
                    decimal tmpWidth = originalImage.Width * newRatio;
                    newWidth = (int)tmpWidth;
                }

                resizedImage = new Bitmap(newWidth, newHeight);
                Graphics g = Graphics.FromImage(resizedImage);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.FillRectangle(Brushes.White, 0, 0, newWidth, newHeight);
                g.DrawImage(originalImage, 0, 0, newWidth, newHeight);
            }
            catch
            {
                return null;
            }
            finally
            {
                originalImage.Dispose();
            }

            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Save(ms, originalFormat);
                return ms.ToArray();
            }
        }
    }
}