﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewReceipt.aspx.cs" Inherits="IS_DigiRacun.NewReceipt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Nov račun - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_products" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT [id], [productName]
FROM [Product]
WHERE [idShop] = @idShop AND [active] = 1
ORDER BY [productName] ASC">
                <SelectParameters>
                    <asp:SessionParameter Name="idShop" SessionField="idShop" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_itemsReceiptTEMP" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT irt.id, irt.seller, irt.idProduct, p.productName, p.price, irt.quantity, p.price * irt.quantity AS 'sumPrice', irt.productSerialNo, irt.dateOfGuarantee
FROM [ItemsReceiptTEMP] irt
INNER JOIN [Product] p ON p.id = irt.idProduct
WHERE seller = @seller"
                DeleteCommand="DELETE FROM [ItemsReceiptTEMP]
WHERE id = @id">
                <DeleteParameters>
                    <asp:Parameter Name="id" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="seller" SessionField="user" />
                </SelectParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniška stran" Value="userForm"></asp:MenuItem>
                            <asp:MenuItem Text="Nazaj na pregled izdanih računov" Value="sellerForm"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">Nov račun</p>
                    <p class="contentH2">Trgovec</p>
                </div>
                <div class="content">
                    <asp:Panel ID="P_createReceipt" runat="server">
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>Trgovina:</p>
                                <p>Št. računa:</p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_shopName" runat="server" ReadOnly="True"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_receiptNumber" runat="server" ReadOnly="True"></asp:TextBox>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>Prodajalec:</p>
                                <p>Datum:</p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_seller" runat="server" ReadOnly="True"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_dateOfPurchase" runat="server" TextMode="Date" ReadOnly="True"></asp:TextBox>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                </div>
                <div class="content">
                    <div class="contentHeader">
                        <p class="contentH1">Upravljanje artiklov</p>
                        <p class="contentH2">Trgovec</p>
                    </div>
                    <asp:Panel ID="P_addProduct" runat="server" DefaultButton="B_addProduct">
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>Izberi artikel:</p>
                                <p>Serijska št.:</p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:DropDownList ID="DDL_products" runat="server" DataSourceID="SDS_products" DataTextField="productName" DataValueField="id" Width="200px"></asp:DropDownList>
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_serialNumber" runat="server"></asp:TextBox>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>Količina:</p>
                                <p>Garancija do:</p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_quantity" runat="server" TextMode="Number" min="1" step="1"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_dateOfGuarantee" runat="server" TextMode="Date"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:Button ID="B_addProduct" runat="server" Text="Dodaj" OnClick="B_addProduct_Click" />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </asp:Panel>
                    <div class="clear"></div>
                    <asp:GridView ID="GV_products" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" Width="961px" DataSourceID="SDS_itemsReceiptTEMP" DataKeyNames="id" ShowFooter="True" OnRowDataBound="GV_products_RowDataBound" OnDataBound="GV_products_DataBound">
                        <Columns>
                            <asp:TemplateField FooterText="Skupaj:">
                                <ItemTemplate>
                                    <asp:Button ID="B_delete" runat="server" CommandName="Delete" Text="Izbriši" />
                                </ItemTemplate>
                                <FooterStyle Font-Bold="True" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                            <asp:BoundField DataField="seller" HeaderText="seller" SortExpression="seller" Visible="False" />
                            <asp:BoundField DataField="idProduct" HeaderText="idProduct" SortExpression="idProduct" Visible="False" />
                            <asp:BoundField DataField="productName" HeaderText="Artikel" SortExpression="productName" />
                            <asp:BoundField DataField="price" HeaderText="Cena" SortExpression="price" DataFormatString="{0:c}" />
                            <asp:BoundField DataField="quantity" HeaderText="Količina" SortExpression="quantity" />
                            <asp:BoundField DataField="productSerialNo" HeaderText="Serijska št." SortExpression="productSerialNo" />
                            <asp:BoundField DataField="dateOfGuarantee" HeaderText="Garancija do" SortExpression="dateOfGuarantee" DataFormatString="{0:dd. MM. yyyy}" />
                            <asp:BoundField DataField="sumPrice" HeaderText="Cena skupaj" ReadOnly="True" SortExpression="sumPrice" DataFormatString="{0:c}" />
                        </Columns>
                    </asp:GridView>

                    <p style="text-align: right; padding-right: 40px; padding-bottom: 20px;">
                        <asp:Button ID="B_deleteAll" runat="server" Text="Izbriši vse" OnClick="B_deleteAll_Click" OnClientClick="return confirm('Ali ste prepričani, da želite izbrisati vse artikle z računa?')" Enabled="False" />
                        &nbsp;
                        <asp:Button ID="B_createReceipt" runat="server" Text="Ustvari račun" OnClick="B_createReceipt_Click" Enabled="False" />
                    </p>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
