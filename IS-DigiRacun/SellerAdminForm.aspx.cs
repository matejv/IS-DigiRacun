﻿using System;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class SellerAdminForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access SellerAdminForm.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false ||
                !userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C"))
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();
                Session["shopId"] = getShopId(Session["user"].ToString());
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "sellerForm":
                    Server.Transfer("SellerForm.aspx");
                    break;
                case "addProduct":
                    Server.Transfer("AddProduct.aspx");
                    break;
            }
        }

        protected void B_addProduct_Click(object sender, EventArgs e)
        {
            Server.Transfer("AddProduct.aspx");
        }

        protected void B_editProduct_Click(object sender, EventArgs e)
        {
            Button btnEdit = (Button)sender;
            GridViewRow gvr = (GridViewRow)btnEdit.NamingContainer;
            Session["productId"] = GV_receipts.DataKeys[gvr.RowIndex].Value.ToString();

            Server.Transfer("AddProduct.aspx");
        }
    }
}