﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserForm.aspx.cs" Inherits="IS_DigiRacun.UserForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Uporabnik - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_receipts" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT id, shop, receiptNumber, dateOfPurchase
FROM [Receipt]
WHERE username = @username AND activeUser = 1
ORDER BY dateOfPurchase DESC, receiptNumber DESC">
                <SelectParameters>
                    <asp:SessionParameter Name="username" SessionField="user" />
                </SelectParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Pregled izdelkov za nakup" Value="shopWindow"></asp:MenuItem>
                            <asp:MenuItem Text="Pregled gradiv izdelkov na računih" Value="userMaterials"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="content">
                    <div class="contentHalf">
                        <asp:Panel ID="P_addByPicture" runat="server" DefaultButton="B_addByPicture">
                            <div class="contentHeader">
                                <p class="contentH1">Dodajanje računa prek slike</p>
                                <p class="contentH2">Uporabnik</p>
                            </div>
                            <div class="contentLeft">
                                <p>
                                    Trgovina:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_shop" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_shop" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_addByPicture"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    Št. računa:
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    Datum nakupa:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_dateOfPurchase" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_dateOfPurchase" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_addByPicture"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    Garancija do:
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    Izberi sliko računa:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_receiptPicture" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="FU_receiptPicture" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_addByPicture"></asp:RequiredFieldValidator>
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_shop" runat="server" ValidationGroup="VG_addByPicture"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_receiptNumber" runat="server" ValidationGroup="VG_addByPicture"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_dateOfPurchase" runat="server" TextMode="Date" ValidationGroup="VG_addByPicture"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_dateOfGuarantee" runat="server" TextMode="Date" ValidationGroup="VG_addByPicture"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:FileUpload ID="FU_receiptPicture" runat="server" />
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:Button ID="B_addByPicture" runat="server" Text="Dodaj" ValidationGroup="VG_addByPicture" OnClick="B_addByPicture_Click" />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </asp:Panel>
                    </div>
                    <div class="contentHalf">
                        <asp:Panel ID="P_addByCode" runat="server" DefaultButton="B_addByCode">
                            <div class="contentHeader">
                                <p class="contentH1">Dodajanje računa prek kode</p>
                                <p class="contentH2">Uporabnik</p>
                            </div>
                            <div class="contentLeft">
                                <p>
                                    Koda računa:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_receiptCode" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_receiptCode" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_addByCode"></asp:RequiredFieldValidator>
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_receiptCode" runat="server" ValidationGroup="VG_addByCode"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:Button ID="B_addByCode" runat="server" Text="Dodaj" ValidationGroup="VG_addByCode" OnClick="B_addByCode_Click" />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </asp:Panel>
                    </div>
                    <div class="clear"></div>
                    <div class="contentHeader">
                        <p class="contentH1">Upravljanje računov</p>
                        <p class="contentH2">Uporabnik</p>
                    </div>
                    <asp:GridView ID="GV_receipts" runat="server" Width="961px" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SDS_receipts" AllowPaging="True" AllowSorting="True" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="B_edit" runat="server" OnClick="B_edit_Click" Text="Uredi" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                            <asp:BoundField DataField="shop" HeaderText="Trgovina" SortExpression="shop" />
                            <asp:BoundField DataField="receiptNumber" HeaderText="Št. računa" SortExpression="receiptNumber" />
                            <asp:BoundField DataField="dateOfPurchase" DataFormatString="{0:dd. MM. yyyy}" HeaderText="Datum nakupa" SortExpression="dateOfPurchase" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
