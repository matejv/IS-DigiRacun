﻿using System;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public class ProductPicture
    {
        public string id { get; set; }
        public string fileName { get; set; }
        private string _material;

        public string material
        {
            get { return "data:image/jpeg;base64," + Convert.ToBase64String(resizeImage(Convert.FromBase64String(_material), 450, 350)); }
            set { _material = value; }
        }
    }
}