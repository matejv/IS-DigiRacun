﻿using System;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class ShopWindow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["userLogged"] != null && (bool)Session["userLogged"])
                {
                    L_user.Text = Session["user"].ToString();
                    L_user.Visible = true;

                    M_mainMenu.Items[0].Text = "Odjava";
                    M_mainMenu.Items[0].Value = "logout";
                    M_mainMenu.Items.Add(new MenuItem("Uporabniški profil", "userProfile"));

                    if (!Session["access"].ToString().ToUpper().Equals("B75E7D25-0296-4AD0-833E-DB234C96151E"))
                    {
                        M_mainMenu.Items.Add(new MenuItem("Uporabniška stran", "userForm"));
                    }

                    M_mainMenu.Items.Add(new MenuItem("Domov", "homeForm"));
                }
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "login":
                    Response.Redirect("Login.aspx");
                    break;
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "homeForm":
                    redirect();
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void LV_allProducts_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                HiddenField hdnMaterial = (HiddenField)e.Item.FindControl("hdnMaterial");
                Image imgControl = (System.Web.UI.WebControls.Image)e.Item.FindControl("IMG_material");
                imgControl.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(resizeImage(Convert.FromBase64String(hdnMaterial.Value), 200, 200));
            }
        }

        protected void LB_item_Click(object sender, EventArgs e)
        {
            LinkButton linkButton = (LinkButton)sender;
            ListViewItem lvi = (ListViewItem)linkButton.NamingContainer;

            Response.Redirect(String.Format("ShopWindowProduct.aspx?id={0}", LV_allProducts.DataKeys[lvi.DataItemIndex].Value.ToString()));
        }
    }
}