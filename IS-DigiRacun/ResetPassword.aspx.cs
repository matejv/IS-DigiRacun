﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        private readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            // when logged user tries to open reset password page is redirected to home page
            if (Session["userLogged"] != null && (bool)Session["userLogged"])
            {
                redirect();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "shopWindow":
                    Server.Transfer("ShopWindow.aspx");
                    break;
                case "login":
                    Response.Redirect("Login.aspx");
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
        }

        protected void B_reset_Click(object sender, EventArgs e)
        {
            if (getEmail(TB_username.Text).Length == 0)
            {
                CV_username.IsValid = false;
                return;
            }

            bool successfull = sendNewPassword(true, TB_username.Text, generateNewPassword());

            if (!successfull)
            {
                Response.Write("<script>alert('Napaka: Pošiljanje e-poštnega sporočila z novim geslom ni uspelo.');</script>");
            }
            else
            {
                Response.Write("<script>alert('E-poštno sporočilo z novim geslom je bilo uspešno poslano.');</script>");
                Response.Redirect("Login.aspx");
            }
        }
    }
}