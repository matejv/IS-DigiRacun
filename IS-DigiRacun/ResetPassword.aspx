﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="IS_DigiRacun.ResetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Ponastavi geslo - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small" Visible="False"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Pregled izdelkov za nakup" Value="shopWindow"></asp:MenuItem>
                            <asp:MenuItem Text="Prijava" Value="login"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <asp:Panel ID="P_reset" runat="server" DefaultButton="B_reset">
                    <div class="contentHeader">
                        <p class="contentH1">Ponastavi geslo</p>
                        <p class="contentH2">Vnesite uporabniško ime in na vaš e-poštni naslov boste prejeli novo geslo.</p>
                    </div>
                    <div class="contentCentralLeft">
                        <p>
                            Uporabniško ime:
                            <br />
                            <asp:RequiredFieldValidator ID="RFV_username" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_username" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_ResetPassword"></asp:RequiredFieldValidator>
                        </p>
                    </div>
                    <div class="contentCentralRight">
                        <p>
                            <asp:TextBox ID="TB_username" runat="server" ValidationGroup="VG_ResetPassword"></asp:TextBox>
                            <br />
                            <asp:CustomValidator ID="CV_username" runat="server" ControlToValidate="TB_username" ErrorMessage="* Neveljavno uporabniško ime" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_ResetPassword"></asp:CustomValidator>
                        </p>
                        <p>
                            <asp:Button ID="B_reset" runat="server" Text="Potrdi" ValidationGroup="VG_ResetPassword" OnClick="B_reset_Click" />
                        </p>
                        <p>
                            <br />
                            <asp:HyperLink ID="HL_login" runat="server" Font-Size="X-Small" NavigateUrl="~/Login.aspx">Vrni se na prijavno stran</asp:HyperLink>
                        </p>
                    </div>
                    <div class="clear"></div>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
