﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewReceiptCreated.aspx.cs" Inherits="IS_DigiRacun.NewReceiptCreated" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Račun ustvarjen - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniška stran" Value="userForm"></asp:MenuItem>
                            <asp:MenuItem Text="Nazaj na pregled izdanih računov" Value="sellerForm"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">Račun ustvarjen</p>
                    <p class="contentH2">Trgovec</p>
                </div>
                <div class="content">
                    <p style="text-align: center; font-weight: bold; font-size: x-large;">
                        <asp:Label ID="L_receiptCode" runat="server"></asp:Label>
                    </p>
                    <p style="text-align: center;">
                        <asp:Image ID="IMG_qrCode" runat="server" />
                    </p>
                </div>
            </div>
            <div class="menuBar">
                <asp:Button ID="B_return" runat="server" Text="NAZAJ" Height="60px" Width="100px" OnClick="B_return_Click" />
            </div>
            <div class="clear"></div>
        </div>
    </form>
</body>
</html>
