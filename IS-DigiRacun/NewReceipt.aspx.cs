﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class NewReceipt : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);
        private double priceTotal = 0.0;

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access NewReceipt.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false ||
                !(userAccess.Equals("B1B443E8-0D7B-4AD3-86E7-9688156A067D") || userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C")))
            {
                Response.Redirect("Login.aspx");
            }

            if (Session["createdReceipts"] == null)
            {
                // dictionary with receipts created in this session to prevent creation of the same receipt with same receipt number many times
                Session["createdReceipts"] = new Dictionary<string, bool>();
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                if (userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C"))
                {
                    M_mainMenu.Items.AddAt(3, new MenuItem("Pregled izdelkov v ponudbi", "sellerAdminForm"));
                }

                // receipt data
                string receiptNumber = generateReceiptNo();
                TB_shopName.Text = getShopName(Session["idShop"].ToString());
                TB_seller.Text = Session["user"].ToString();
                TB_receiptNumber.Text = receiptNumber;
                TB_dateOfPurchase.Text = DateTime.Today.ToString("yyyy-MM-dd");
                ((Dictionary<string, bool>)Session["createdReceipts"]).Add(receiptNumber, false);

                updateGV_products();
            }
            else
            {
                if (((Dictionary<string, bool>)Session["createdReceipts"])[TB_receiptNumber.Text])
                {
                    Response.Redirect("SellerForm.aspx");
                }
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "sellerForm":
                    Server.Transfer("SellerForm.aspx");
                    break;
                case "sellerAdminForm":
                    Server.Transfer("SellerAdminForm.aspx");
                    break;
            }
        }

        private void updateGV_products()
        {
            GV_products.DataBind();
            DDL_products.SelectedIndex = 0;
            TB_quantity.Text = "1";
            TB_serialNumber.Text = string.Empty;
            TB_dateOfGuarantee.Text = DateTime.Today.AddYears(2).ToString("yyyy-MM-dd");
        }

        private string generateReceiptNo()
        {
            string partIdShop = Session["idShop"].ToString().Substring(Session["idShop"].ToString().Length - 4).ToUpper();

            SqlCommand getLastReceiptNo = new SqlCommand("SELECT receiptNumber FROM [Receipt] WHERE MONTH(dateOfPurchase) = @month AND YEAR(dateOfPurchase) = @year AND code IS NOT NULL AND receiptNumber LIKE @partIdShop ORDER BY receiptNumber DESC", connection);
            getLastReceiptNo.Parameters.AddWithValue("@month", DateTime.Today.Month);
            getLastReceiptNo.Parameters.AddWithValue("@year", DateTime.Today.Year);
            getLastReceiptNo.Parameters.AddWithValue("@partIdShop", partIdShop + "%"); // % because of LIKE operator, starts with value of partIdShop

            connection.Open();
            string lastReceiptNo = getLastReceiptNo.ExecuteScalar() as string;
            connection.Close();

            StringBuilder receiptNo = new StringBuilder();
            receiptNo.Append(partIdShop);
            receiptNo.Append("-");
            receiptNo.Append(DateTime.Today.ToString("MM"));
            receiptNo.Append(DateTime.Today.ToString("yy"));
            receiptNo.Append("-");

            if (lastReceiptNo == null || lastReceiptNo.Length < 7)
                receiptNo.Append("0000000");
            else
            {
                try
                {
                    int lastNo = Convert.ToInt32(lastReceiptNo.Substring(lastReceiptNo.Length - 7));
                    lastNo++;
                    receiptNo.Append(String.Format("{0:D7}", lastNo));
                }
                catch (Exception)
                {
                    receiptNo.Append("0000000");
                }
            }

            return receiptNo.ToString();
        }

        protected void B_addProduct_Click(object sender, EventArgs e)
        {
            SqlCommand addProduct = new SqlCommand("INSERT INTO [ItemsReceiptTEMP](id, seller, idProduct, quantity, productSerialNo, dateOfGuarantee) VALUES (NEWID(), @seller, @idProduct, @quantity, @productSerialNo, @dateOfGuarantee)", connection);
            addProduct.Parameters.AddWithValue("@seller", Session["user"].ToString());
            addProduct.Parameters.AddWithValue("@idProduct", DDL_products.SelectedValue);
            addProduct.Parameters.AddWithValue("@quantity", Convert.ToInt32(TB_quantity.Text));
            addProduct.Parameters.AddWithValue("@productSerialNo", TB_serialNumber.Text);
            addProduct.Parameters.AddWithValue("@dateOfGuarantee", TB_dateOfGuarantee.Text);

            connection.Open();
            addProduct.ExecuteNonQuery();
            connection.Close();

            updateGV_products();
        }

        protected void B_deleteAll_Click(object sender, EventArgs e)
        {
            SqlCommand deleteAllFromReceipt = new SqlCommand("DELETE FROM [ItemsReceiptTEMP] WHERE seller = @seller", connection);
            deleteAllFromReceipt.Parameters.AddWithValue("@seller", Session["user"].ToString());

            connection.Open();
            deleteAllFromReceipt.ExecuteNonQuery();
            connection.Close();

            updateGV_products();
        }

        protected void GV_products_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // check row type
            if (e.Row.RowType == DataControlRowType.DataRow)
                // if row type is DataRow, add sumPrice value to totalPrice
                priceTotal += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "sumPrice"));
            else if (e.Row.RowType == DataControlRowType.Footer)
                // if row type is footer, show calculated total value
                e.Row.Cells[9].Text = String.Format("{0:c}", priceTotal);
        }

        private string generateCode()
        {
            Random rnd = new Random();

            StringBuilder code = new StringBuilder();
            code.Append(Session["idShop"].ToString().Substring(Session["idShop"].ToString().Length - 4).ToUpper());
            code.Append(DateTime.Now.ToString("HH"));
            code.Append(DateTime.Now.ToString("mm"));
            code.Append(DateTime.Today.ToString("dd"));
            code.Append(DateTime.Today.ToString("MM"));
            code.Append(DateTime.Today.ToString("yy"));
            code.Append(rnd.Next(100, 999));

            return code.ToString();
        }

        protected void B_createReceipt_Click(object sender, EventArgs e)
        {
            if (GV_products.Rows.Count == 0)
            {
                Response.Write("<script>alert('Napaka: Na računu ni artiklov.');</script>");
                return;
            }

            string receiptID = getNewId();
            string receiptCode = generateCode();

            SqlCommand createReceipt = new SqlCommand("INSERT INTO [Receipt](id, shop, receiptNumber, dateOfPurchase, dateOfGuarantee, code, seller, activeUser, activeShop) VALUES (@id, @shop, @receiptNumber, @dateOfPurchase, @dateOfGuarantee, @code, @seller, 1, 1)", connection);
            createReceipt.Parameters.AddWithValue("@id", receiptID);
            createReceipt.Parameters.AddWithValue("@shop", TB_shopName.Text);
            createReceipt.Parameters.AddWithValue("@receiptNumber", TB_receiptNumber.Text);
            createReceipt.Parameters.AddWithValue("@dateOfPurchase", TB_dateOfPurchase.Text);
            createReceipt.Parameters.AddWithValue("@dateOfGuarantee", TB_dateOfGuarantee.Text);
            createReceipt.Parameters.AddWithValue("@code", receiptCode);
            createReceipt.Parameters.AddWithValue("@seller", TB_seller.Text);

            SqlCommand getItemsOnReceipt = new SqlCommand("SELECT irt.idProduct, irt.productSerialNo, p.price, irt.dateOfGuarantee, irt.quantity FROM [ItemsReceiptTEMP] irt INNER JOIN [Product] p ON p.id = irt.idProduct WHERE irt.seller = @seller", connection);
            getItemsOnReceipt.Parameters.AddWithValue("@seller", Session["user"].ToString());

            SqlCommand removeTempItems = new SqlCommand("DELETE FROM [ItemsReceiptTEMP] WHERE seller = @seller", connection);
            removeTempItems.Parameters.AddWithValue("@seller", TB_seller.Text);

            connection.Open();
            createReceipt.ExecuteNonQuery();
            SqlDataReader reader = getItemsOnReceipt.ExecuteReader();

            while (reader.Read())
            {
                SqlCommand addItemToReceipt = new SqlCommand("INSERT INTO [ReceiptProduct](id, idReceipt, idProduct, productSerialNo, priceUponPurchase, dateOfGuarantee, quantity) VALUES (NEWID(), @idReceipt, @idProduct, @productSerialNo, @priceUponPurchase, @dateOfGuarantee, @quantity)", connection);
                addItemToReceipt.Parameters.AddWithValue("@idReceipt", receiptID);
                addItemToReceipt.Parameters.AddWithValue("@idProduct", reader.GetGuid(0).ToString());
                addItemToReceipt.Parameters.AddWithValue("@productSerialNo", reader.IsDBNull(1) ? null : reader.GetString(1));
                addItemToReceipt.Parameters.AddWithValue("@priceUponPurchase", reader.GetDouble(2));
                addItemToReceipt.Parameters.AddWithValue("@dateOfGuarantee", reader.GetDateTime(3));
                addItemToReceipt.Parameters.AddWithValue("@quantity", reader.GetInt32(4));

                addItemToReceipt.ExecuteNonQuery();
            }

            reader.Close();

            removeTempItems.ExecuteNonQuery();
            connection.Close();

            Session["receiptCode"] = receiptCode;
            ((Dictionary<string, bool>)Session["createdReceipts"])[TB_receiptNumber.Text] = true;
            Response.Redirect("NewReceiptCreated.aspx");
        }

        protected void GV_products_DataBound(object sender, EventArgs e)
        {
            if (GV_products.Rows.Count > 0)
            {
                B_createReceipt.Enabled = true;
                B_deleteAll.Enabled = true;
            }
            else
            {
                B_createReceipt.Enabled = false;
                B_deleteAll.Enabled = false;
            }
        }
    }
}