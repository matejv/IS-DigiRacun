﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public class Service1 : IService1
    {
        private readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);
        private WebOperationContext ctx = WebOperationContext.Current;
        private string loggedUser;

        // USER
        // performs user registration and verifies if it was successful
        public async Task userRegistration(Stream body)
        {
            string data = await new StreamReader(body).ReadToEndAsync();
            JavaScriptSerializer j = new JavaScriptSerializer();
            User user = (User)j.Deserialize(data, typeof(User));

            bool successfull = BaseClass.userRegistration(user.username, user.password, user.email, user.name, user.surname);

            if (!successfull)
                throw new WebFaultException(HttpStatusCode.Conflict);
        }

        // user authentication
        public bool authenticateUser()
        {
            string authHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];

            if (authHeader == null)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return false;
            }

            string[] loginData = authHeader.Split(':');
            loggedUser = loginData[0];

            if (loginData.Length == 2 && checkLogin(loginData[0], loginData[1]))
                return true;

            ctx.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
            return false;
        }

        // edit data of the user
        public async Task editUser(Stream body)
        {
            string data = await new StreamReader(body).ReadToEndAsync();
            JavaScriptSerializer j = new JavaScriptSerializer();
            User user = (User)j.Deserialize(data, typeof(User));

            if (!authenticateUser() || !user.username.Equals(loggedUser))
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            try
            {
                SqlCommand editUser = new SqlCommand("UPDATE [User] SET email = @email, name = @name, surname = @surname WHERE username = @username AND active = 1", connection);
                editUser.Parameters.AddWithValue("@username", user.username);
                editUser.Parameters.AddWithValue("@email", user.email);

                if (user.name != null)
                {
                    editUser.Parameters.AddWithValue("@name", user.name);
                }
                else
                {
                    editUser.Parameters.AddWithValue("@name", DBNull.Value);
                }

                if (user.surname != null)
                {
                    editUser.Parameters.AddWithValue("@surname", user.surname);
                }
                else
                {
                    editUser.Parameters.AddWithValue("@surname", DBNull.Value);
                }

                connection.Open();
                editUser.ExecuteNonQuery();
            }
            catch
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }
            finally
            {
                connection.Close();
            }
        }

        // change user's password
        public async Task changeUserPassword(Stream body)
        {
            if (!authenticateUser())
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            string password = await new StreamReader(body).ReadToEndAsync();

            SqlCommand changePassword = new SqlCommand("UPDATE [User] SET password = @password WHERE username = @username AND active = 1", connection);
            changePassword.Parameters.AddWithValue("@username", loggedUser);
            changePassword.Parameters.AddWithValue("@password", passwordExtract(password));

            try
            {
                connection.Open();
                changePassword.ExecuteNonQuery();
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }
            finally
            {
                connection.Close();
            }
        }

        // get user data
        public async Task<User> getUser()
        {
            if (!authenticateUser())
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            User user = null;

            SqlCommand userData = new SqlCommand("SELECT username, email, name, surname FROM [User] WHERE username = @username COLLATE SQL_Latin1_General_CP1_CS_AS AND active = 1", connection);
            userData.Parameters.AddWithValue("@username", loggedUser);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await userData.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    reader.Read();

                    user = new User
                    {
                        username = reader.GetString(0),
                        email = reader.GetString(1),
                        name = reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                        surname = reader.IsDBNull(3) ? string.Empty : reader.GetString(3)
                    };
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return user;
        }

        // reset user's password
        public async Task resetPassword(Stream body)
        {
            string username = await new StreamReader(body).ReadToEndAsync();

            if (getEmail(username).Length == 0)
                throw new WebFaultException(HttpStatusCode.NotFound);

            bool successfull = sendNewPassword(true, username, generateNewPassword());

            if (!successfull)
                throw new WebFaultException(HttpStatusCode.InternalServerError);
        }

        // RECEIPT
        // converts received string to DateTime
        private DateTime toDate(string date)
        {
            try
            {
                return DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        private string usernameReceipt(string idReceipt)
        {
            string username = "";
            SqlCommand getUsername = new SqlCommand("SELECT username FROM [Receipt] WHERE id = @id", connection);
            getUsername.Parameters.AddWithValue("@id", idReceipt);

            connection.Open();

            try
            {
                username = getUsername.ExecuteScalar().ToString();
            }
            catch (NullReferenceException)
            {
                throw new WebFaultException(HttpStatusCode.NotFound);
            }
            catch (SqlException)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            finally
            {
                connection.Close();
            }

            return username;
        }

        public async Task addReceiptPicture(Stream body)
        {
            if (!authenticateUser())
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            string data = await new StreamReader(body).ReadToEndAsync();
            JavaScriptSerializer j = new JavaScriptSerializer();
            Receipt receipt = (Receipt)j.Deserialize(data, typeof(Receipt));
            byte[] file = Convert.FromBase64String(receipt.receiptPicture);
            string fileExtension = Path.GetExtension(receipt.receiptPicFileName);

            SqlCommand newReceiptPicture = new SqlCommand("INSERT INTO [Receipt](id, username, shop, receiptNumber, dateOfPurchase, dateOfGuarantee, receiptPicFileName, receiptPicture, activeUser, activeShop) VALUES(NEWID(), @username, @shop, @receiptNumber, @dateOfPurchase, @dateOfGuarantee, @receiptPicFileName, @receiptPicture, 1, 0)", connection);
            newReceiptPicture.Parameters.AddWithValue("@username", loggedUser);
            newReceiptPicture.Parameters.AddWithValue("@shop", receipt.shop);
            newReceiptPicture.Parameters.AddWithValue("@dateOfPurchase", toDate(receipt.dateOfPurchase));
            newReceiptPicture.Parameters.AddWithValue("@receiptPicFileName", receipt.receiptPicFileName);

            if (receipt.receiptNumber != null)
            {
                newReceiptPicture.Parameters.AddWithValue("@receiptNumber", receipt.receiptNumber);
            }
            else
            {
                newReceiptPicture.Parameters.AddWithValue("@receiptNumber", DBNull.Value);
            }

            if (receipt.dateOfGuarantee != null)
            {
                newReceiptPicture.Parameters.AddWithValue("@dateOfGuarantee", toDate(receipt.dateOfGuarantee));
            }
            else
            {
                newReceiptPicture.Parameters.AddWithValue("@dateOfGuarantee", DBNull.Value);
            }

            if (fileExtension.ToLower().Equals(".jpg") || fileExtension.ToLower().Equals(".jpeg") || fileExtension.ToLower().Equals(".png"))
            {
                newReceiptPicture.Parameters.AddWithValue("@receiptPicture", Convert.ToBase64String(resizeImage(file, 1000, 1000)));
            }
            else
            {
                throw new WebFaultException(HttpStatusCode.UnsupportedMediaType);
            }


            try
            {
                connection.Open();
                newReceiptPicture.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            finally
            {
                connection.Close();
            }
        }

        public async Task addReceiptCode(Stream body)
        {
            if (!authenticateUser())
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            string code = await new StreamReader(body).ReadToEndAsync();
            string receiptId = string.Empty;

            SqlCommand checkCode = new SqlCommand("SELECT id FROM [Receipt] WHERE code = @code AND username IS NULL AND activeUser = 1 AND activeShop = 1", connection);
            checkCode.Parameters.AddWithValue("@code", code);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await checkCode.ExecuteReaderAsync();

                if (reader.Read())
                {
                    receiptId = reader.IsDBNull(0) ? string.Empty : reader.GetGuid(0).ToString();
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            if (receiptId.Length == 0)
                throw new WebFaultException(HttpStatusCode.NotFound);

            SqlCommand newReceiptCode = new SqlCommand("UPDATE [Receipt] SET username = @username WHERE id = @id", connection);
            newReceiptCode.Parameters.AddWithValue("@username", loggedUser);
            newReceiptCode.Parameters.AddWithValue("@id", receiptId);

            try
            {
                connection.Open();
                newReceiptCode.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            finally
            {
                connection.Close();
            }
        }

        public async Task<List<Receipt>> getReceipts()
        {
            if (!authenticateUser())
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            List<Receipt> receipts = new List<Receipt>();

            SqlCommand getReceipts = new SqlCommand("SELECT id, shop, receiptNumber, dateOfPurchase, code FROM [Receipt] WHERE username = @username AND activeUser = 1 ORDER BY dateOfPurchase DESC", connection);
            getReceipts.Parameters.AddWithValue("@username", loggedUser);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getReceipts.ExecuteReaderAsync();

                while (reader.Read())
                {
                    receipts.Add(new Receipt
                    {
                        id = reader.GetGuid(0).ToString(),
                        shop = reader.GetString(1),
                        receiptNumber = reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                        dateOfPurchase = reader.GetDateTime(3).ToString("ddMMyyyy"),
                        code = reader.IsDBNull(4) ? string.Empty : reader.GetString(4) // attribute code tells mobile application which layout for receipt details should prepare
                    });
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return receipts;
        }

        public async Task<Receipt> getReceipt(string receiptId)
        {
            if (!authenticateUser() || !usernameReceipt(receiptId).Equals(loggedUser))
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            Receipt receipt = null;

            SqlCommand getReceipt = new SqlCommand("SELECT id, username, shop, receiptNumber, dateOfPurchase, dateOfGuarantee, receiptPicture, receiptPicFileName, code FROM [Receipt] WHERE id = @id AND activeUser = 1", connection);
            getReceipt.Parameters.AddWithValue("@id", receiptId);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getReceipt.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    reader.Read();

                    receipt = new Receipt
                    {
                        id = reader.GetGuid(0).ToString(),
                        username = reader.GetString(1),
                        shop = reader.GetString(2),
                        receiptNumber = reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                        dateOfPurchase = reader.GetDateTime(4).ToString("ddMMyyyy"),
                        dateOfGuarantee = reader.IsDBNull(5) ? string.Empty : reader.GetDateTime(5).ToString("ddMMyyyy"),
                        receiptPicture = reader.IsDBNull(6) ? string.Empty : reader.GetString(6),
                        receiptPicFileName = reader.IsDBNull(7) ? string.Empty : reader.GetString(7),
                        code = reader.IsDBNull(8) ? string.Empty : reader.GetString(8)
                    };
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return receipt;
        }

        public async Task updateReceipt(Stream body)
        {
            string data = await new StreamReader(body).ReadToEndAsync();
            JavaScriptSerializer j = new JavaScriptSerializer();
            Receipt receipt = (Receipt)j.Deserialize(data, typeof(Receipt));

            if (!authenticateUser() || !usernameReceipt(receipt.id).Equals(loggedUser))
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            SqlCommand updateReceipt = new SqlCommand("UPDATE [Receipt] SET shop = @shop, receiptNumber = @receiptNumber, dateOfPurchase = @dateOfPurchase, dateOfGuarantee = @dateOfGuarantee WHERE id = @id AND code IS NULL AND activeUser = 1", connection);
            updateReceipt.Parameters.AddWithValue("@shop", receipt.shop);
            updateReceipt.Parameters.AddWithValue("@dateOfPurchase", toDate(receipt.dateOfPurchase));
            updateReceipt.Parameters.AddWithValue("@id", receipt.id);

            if (receipt.receiptNumber != null)
            {
                updateReceipt.Parameters.AddWithValue("@receiptNumber", receipt.receiptNumber);
            }
            else
            {
                updateReceipt.Parameters.AddWithValue("@receiptNumber", DBNull.Value);
            }

            if (receipt.dateOfGuarantee != null)
            {
                updateReceipt.Parameters.AddWithValue("@dateOfGuarantee", toDate(receipt.dateOfGuarantee));
            }
            else
            {
                updateReceipt.Parameters.AddWithValue("@dateOfGuarantee", DBNull.Value);
            }

            try
            {
                connection.Open();
                updateReceipt.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            finally
            {
                connection.Close();
            }
        }

        public async Task deleteReceipt(Stream body)
        {
            string receiptId = await new StreamReader(body).ReadToEndAsync();

            if (!authenticateUser() || !usernameReceipt(receiptId).Equals(loggedUser))
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            SqlCommand deleteReceipt = new SqlCommand("UPDATE [Receipt] SET activeUser = 0 WHERE id = @id", connection);
            deleteReceipt.Parameters.AddWithValue("@id", receiptId);

            try
            {
                connection.Open();
                deleteReceipt.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            finally
            {
                connection.Close();
            }
        }

        // PRODUCT
        public async Task<List<Product>> getProductsOnReceipt(string receiptId)
        {
            if (!authenticateUser() || !usernameReceipt(receiptId).Equals(loggedUser))
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            List<Product> products = new List<Product>();

            SqlCommand getProducts = new SqlCommand("SELECT rp.id, p.productName, rp.priceUponPurchase, rp.quantity, rp.productSerialNo, rp.dateOfGuarantee FROM [Receipt] r INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id INNER JOIN [Product] p ON p.id = rp.idProduct WHERE r.id = @receiptId ORDER BY p.productName ASC", connection);
            getProducts.Parameters.AddWithValue("@receiptId", receiptId);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getProducts.ExecuteReaderAsync();

                while (reader.Read())
                {
                    products.Add(new Product
                    {
                        id = reader.GetGuid(0).ToString(),
                        productName = reader.GetString(1),
                        price = reader.GetDouble(2),
                        quantity = reader.GetInt32(3),
                        serialNo = reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                        dateOfGuarantee = reader.IsDBNull(5) ? string.Empty : reader.GetDateTime(5).ToString("ddMMyyyy")
                    });
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return products;
        }

        public async Task<List<Product>> getProductsShopWindow()
        {
            List<Product> products = new List<Product>();

            SqlCommand getProducts = new SqlCommand("SELECT id, productName, price, name, material FROM (SELECT DISTINCT p.id, p.productName, p.price, s.name, m.material, ROW_NUMBER() OVER(PARTITION BY p.id ORDER BY m.sortOrder ASC) rn FROM [Product] p INNER JOIN [Shop] s ON s.id = p.idShop INNER JOIN [Materials] m ON m.idProduct = p.id WHERE p.active = 1 AND m.active = 1 AND m.picture = 1) x WHERE rn = 1", connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getProducts.ExecuteReaderAsync();

                while (reader.Read())
                {
                    products.Add(new Product
                    {
                        id = reader.GetGuid(0).ToString(),
                        productName = reader.GetString(1).ToString(),
                        price = reader.GetDouble(2),
                        shop = reader.GetString(3).ToString(),
                        coverPicture = reader.GetString(4).ToString()
                    });
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return products;
        }

        public async Task<ProductShop> getProductShopWindow(string idProduct)
        {
            ProductShop productShop = null;

            SqlCommand getProductData = new SqlCommand("SELECT productName, description, techDetails, price, pc.category, s.name, s.address, s.webpage, s.phone, s.email FROM [Product] p INNER JOIN [ProductCategory] pc ON pc.id = p.idCategory INNER JOIN [Shop] s ON s.id = p.idShop WHERE p.id = @idProduct AND p.active = 1", connection);
            getProductData.Parameters.AddWithValue("@idProduct", idProduct);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getProductData.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    reader.Read();

                    productShop = new ProductShop
                    {
                        productName = reader.GetString(0),
                        description = reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                        techDetails = reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                        price = reader.GetDouble(3),
                        category = reader.GetString(4),
                        shopName = reader.GetString(5),
                        shopAddress = reader.GetString(6),
                        shopWebpage = reader.IsDBNull(7) ? string.Empty : reader.GetString(7),
                        shopPhone = reader.IsDBNull(8) ? string.Empty : reader.GetString(8),
                        shopEmail = reader.IsDBNull(9) ? string.Empty : reader.GetString(9)
                    };
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return productShop;
        }

        public async Task<List<ProductCategory>> getProductCategories()
        {
            List<ProductCategory> productCategories = new List<ProductCategory>();

            SqlCommand getProductCategories = new SqlCommand("SELECT id, category FROM [ProductCategory] ORDER BY category ASC", connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getProductCategories.ExecuteReaderAsync();

                while (reader.Read())
                {
                    productCategories.Add(new ProductCategory
                    {
                        id = reader.GetGuid(0).ToString(),
                        category = reader.GetString(1)
                    });
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return productCategories;
        }

        // MATERIALS
        public async Task<List<Material>> getMaterialsOnReceipt(string receiptId)
        {
            if (!authenticateUser() || !usernameReceipt(receiptId).Equals(loggedUser))
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            List<Material> materials = new List<Material>();

            SqlCommand getMaterials = new SqlCommand("SELECT DISTINCT p.id AS 'productId', p.productName, m.id AS 'materialId', m.material, m.fileName, m.description, m.picture FROM [Receipt] r INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id INNER JOIN [Product] p ON p.id = rp.idProduct INNER JOIN [Materials] m ON m.idProduct = p.id WHERE r.id = @receiptId AND m.active = 1 ORDER BY p.productName ASC", connection);
            getMaterials.Parameters.AddWithValue("@receiptId", receiptId);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getMaterials.ExecuteReaderAsync();

                while (reader.Read())
                {
                    materials.Add(new Material
                    {
                        productId = reader.GetGuid(0).ToString(),
                        productName = reader.GetString(1),
                        materialId = reader.GetGuid(2).ToString(),
                        material = reader.GetString(3),
                        fileName = reader.GetString(4),
                        description = reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                        picture = reader.GetBoolean(6)
                    });
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return materials;
        }

        private string userMaterialsFilterQuery(string[] categoryIds)
        {
            string defaultQuery = "SELECT DISTINCT r.shop, r.receiptNumber, r.dateOfPurchase, p.productName, m.material, m.fileName, m.description, m.picture" +
            " FROM [Receipt] r" +
            " INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id" +
            " INNER JOIN [Product] p ON p.id = rp.idProduct" +
            " INNER JOIN [Materials] m ON m.idProduct = p.id" +
            " WHERE r.username = @username AND r.activeUser = 1 AND m.active = 1 {0}" +
            " ORDER BY dateOfPurchase DESC, receiptNumber DESC, productName ASC";

            StringBuilder filterQuery = new StringBuilder();
            bool firstCategoryChecked = false;
            bool closeFilters = false;

            foreach (string categoryId in categoryIds)
            {
                closeFilters = true;

                if (!firstCategoryChecked)
                {
                    firstCategoryChecked = true;

                    filterQuery.Append(" AND (p.idCategory = '");
                }
                else
                {
                    filterQuery.Append(" OR p.idCategory = '");
                }

                filterQuery.Append(categoryId);
                filterQuery.Append("'");
            }

            if (closeFilters)
            {
                filterQuery.Append(")");
            }

            return String.Format(defaultQuery, filterQuery.ToString());
        }

        public async Task<List<Material>> getUserMaterials(string productCategoryIds)
        {
            if (!authenticateUser())
                throw new WebFaultException(HttpStatusCode.Unauthorized);

            List<Material> materials = new List<Material>();
            string[] categoryIds = productCategoryIds.Split('$');

            if (categoryIds.Length == 1 && categoryIds[0].ToLower().Equals("all"))
            {
                categoryIds = new string[0];
            }

            SqlCommand getMaterials = new SqlCommand(userMaterialsFilterQuery(categoryIds), connection);
            getMaterials.Parameters.AddWithValue("@username", loggedUser);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getMaterials.ExecuteReaderAsync();

                while (reader.Read())
                {
                    materials.Add(new Material
                    {
                        shop = reader.GetString(0),
                        receiptNumber = reader.GetString(1),
                        dateOfPurchase = reader.GetDateTime(2).ToString("ddMMyyyy"),
                        productName = reader.GetString(3),
                        material = reader.GetString(4),
                        fileName = reader.GetString(5),
                        description = reader.IsDBNull(6) ? string.Empty : reader.GetString(6),
                        picture = reader.GetBoolean(7)
                    });
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return materials;
        }

        public async Task<List<Material>> getProductPicturesShopWindow(string idProduct)
        {
            List<Material> productPictures = new List<Material>();

            SqlCommand getPictures = new SqlCommand("SELECT id, fileName, material FROM [Materials] WHERE idProduct = @idProduct AND picture = 1 AND active = 1 ORDER BY sortOrder ASC", connection);
            getPictures.Parameters.AddWithValue("@idProduct", idProduct);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = await getPictures.ExecuteReaderAsync();

                while (reader.Read())
                {
                    productPictures.Add(new Material
                    {
                        materialId = reader.GetGuid(0).ToString(),
                        fileName = reader.GetString(1),
                        material = reader.GetString(2)
                    });
                }
            }
            catch (Exception)
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close();
            }

            return productPictures;
        }
    }
}
