﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class AddProduct : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access AddProduct.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false ||
                !userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C"))
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                updateGV_materials();

                if (Session["productId"] != null)
                {
                    L_pageTitle.Text = "Uredi izdelek";
                    B_addProduct.Visible = false;
                    B_save.Visible = true;
                    P_addProduct.DefaultButton = "B_save";
                    getExistingProduct();
                }
                else
                {
                    FU_uploadFile.Enabled = false;
                    TB_fDescription.Enabled = false;
                    TB_fSortOrder.Enabled = false;
                    B_uploadFile.Enabled = false;
                }
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "sellerForm":
                    Server.Transfer("SellerForm.aspx");
                    break;
                case "sellerAdminForm":
                    Session["productId"] = null;
                    Server.Transfer("SellerAdminForm.aspx");
                    break;
            }
        }

        private void updateGV_materials()
        {
            GV_materials.DataBind();
            TB_fDescription.Text = string.Empty;
            TB_fSortOrder.Text = "1";
        }

        private void getExistingProduct()
        {
            SqlCommand getProductData = new SqlCommand("SELECT productName, description, techDetails, price, idCategory, active FROM [Product] WHERE id = @id", connection);
            getProductData.Parameters.AddWithValue("@id", Session["productId"].ToString());

            connection.Open();
            SqlDataReader reader = getProductData.ExecuteReader();

            if (reader.Read())
            {
                TB_productName.Text = reader.GetString(0);
                TA_description.Value = reader.GetString(1);
                TA_techDetails.Value = reader.GetString(2);
                TB_price.Text = reader.GetDouble(3).ToString();
                DDL_productCategory.SelectedValue = reader.GetGuid(4).ToString();
                CB_active.Checked = reader.GetBoolean(5);
            }

            reader.Close();
            connection.Close();
        }

        protected void B_addProduct_Click(object sender, EventArgs e)
        {
            Session["productId"] = getNewId();

            SqlCommand addProduct = new SqlCommand("INSERT INTO [Product](id, productName, description, techDetails, price, active, idShop, idCategory) VALUES (@id, @productName, @description, @techDetails, @price, @active, @idShop, @idCategory)", connection);
            addProduct.Parameters.AddWithValue("@id", Session["productId"].ToString());
            addProduct.Parameters.AddWithValue("@productName", TB_productName.Text);
            addProduct.Parameters.AddWithValue("@description", TA_description.Value);
            addProduct.Parameters.AddWithValue("@techDetails", TA_techDetails.Value);
            addProduct.Parameters.AddWithValue("@price", Convert.ToDouble(TB_price.Text));
            addProduct.Parameters.AddWithValue("@active", CB_active.Checked);
            addProduct.Parameters.AddWithValue("@idShop", Session["shopId"].ToString());
            addProduct.Parameters.AddWithValue("@idCategory", DDL_productCategory.SelectedValue);

            connection.Open();
            addProduct.ExecuteNonQuery();
            connection.Close();

            B_addProduct.Visible = false;
            B_save.Visible = true;
            P_addProduct.DefaultButton = "B_save";

            FU_uploadFile.Enabled = true;
            TB_fDescription.Enabled = true;
            TB_fSortOrder.Enabled = true;
            B_uploadFile.Enabled = true;
        }

        protected void B_save_Click(object sender, EventArgs e)
        {
            SqlCommand updateProduct = new SqlCommand("UPDATE [Product] SET productName = @productName, description = @description, techDetails = @techDetails, price = @price, idCategory = @idCategory, active = @active WHERE id = @id", connection);
            updateProduct.Parameters.AddWithValue("@productName", TB_productName.Text);
            updateProduct.Parameters.AddWithValue("@description", TA_description.Value);
            updateProduct.Parameters.AddWithValue("@techDetails", TA_techDetails.Value);
            updateProduct.Parameters.AddWithValue("@price", Convert.ToDouble(TB_price.Text));
            updateProduct.Parameters.AddWithValue("@idCategory", DDL_productCategory.SelectedValue);
            updateProduct.Parameters.AddWithValue("@active", CB_active.Checked);
            updateProduct.Parameters.AddWithValue("@idShop", Session["shopId"].ToString());
            updateProduct.Parameters.AddWithValue("@id", Session["productId"].ToString());

            connection.Open();
            updateProduct.ExecuteNonQuery();
            connection.Close();

            getExistingProduct();
        }

        protected void B_cancel_Click(object sender, EventArgs e)
        {
            Session["productId"] = null;

            Server.Transfer("SellerAdminForm.aspx");
        }

        protected void B_uploadFile_Click(object sender, EventArgs e)
        {
            byte[] file = FU_uploadFile.FileBytes;
            string fileExtension = Path.GetExtension(FU_uploadFile.FileName);

            SqlCommand addMaterial = new SqlCommand("INSERT INTO [Materials](id, idProduct, material, description, picture, active, sortOrder, fileName) VALUES (NEWID(), @idProduct, @material, @description, @picture, @active, @sortOrder, @fileName)", connection);
            addMaterial.Parameters.AddWithValue("@idProduct", Session["productId"].ToString());
            addMaterial.Parameters.AddWithValue("@description", TB_fDescription.Text);
            addMaterial.Parameters.AddWithValue("@active", 1);
            addMaterial.Parameters.AddWithValue("@sortOrder", TB_fSortOrder.Text);
            addMaterial.Parameters.AddWithValue("@fileName", FU_uploadFile.FileName);

            if (fileExtension.ToLower().Equals(".jpg") || fileExtension.ToLower().Equals(".jpeg") || fileExtension.ToLower().Equals(".png"))
            {
                addMaterial.Parameters.AddWithValue("@material", Convert.ToBase64String(resizeImage(file, 800, 800)));
                addMaterial.Parameters.AddWithValue("@picture", 1);
            }
            else if (fileExtension.ToLower().Equals(".pdf"))
            {
                addMaterial.Parameters.AddWithValue("@material", Convert.ToBase64String(file));
                addMaterial.Parameters.AddWithValue("@picture", 0);
            }
            else
            {
                Response.Write("<script>alert('Napaka: Nepodprt format datoteke. Podprti formati so: JPG, JPEG, PNG in PDF.');</script>");
                return;
            }

            connection.Open();
            addMaterial.ExecuteNonQuery();
            connection.Close();

            updateGV_materials();
        }

        protected void GV_materials_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnPicture = (HiddenField)e.Row.FindControl("hdnPicture");

                if (Convert.ToBoolean(hdnPicture.Value))
                {
                    HiddenField hdnMaterial = (HiddenField)e.Row.FindControl("hdnMaterial");
                    System.Web.UI.WebControls.Image imgControl = (System.Web.UI.WebControls.Image)e.Row.Cells[2].FindControl("IMG_material");
                    imgControl.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(resizeImage(Convert.FromBase64String(hdnMaterial.Value), 250, 200));
                }
                else
                {
                    System.Web.UI.WebControls.Image imgControl = (System.Web.UI.WebControls.Image)e.Row.Cells[2].FindControl("IMG_material");
                    imgControl.ImageUrl = "Resources/ic_picture_as_pdf_black_48dp_2x.png";
                }
            }
        }

        protected void B_downloadFile_Click(object sender, EventArgs e)
        {
            Button btnDownload = (Button)sender;
            GridViewRow gvr = (GridViewRow)btnDownload.NamingContainer;
            HiddenField hdnFileName = (HiddenField)gvr.FindControl("hdnFileName");
            HiddenField hdnMaterial = (HiddenField)gvr.FindControl("hdnMaterial");

            Response.AddHeader("Content-disposition", "attachment; filename=" + hdnFileName.Value);
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(Convert.FromBase64String(hdnMaterial.Value));
            Response.End();
        }
    }
}