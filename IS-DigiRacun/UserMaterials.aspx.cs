﻿using System;
using System.Data.SqlClient;
using System.Text;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class UserMaterials : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);

        private static readonly string defaultQuery = "SELECT DISTINCT r.shop, r.receiptNumber, r.dateOfPurchase, p.productName, m.material, m.fileName, m.description, m.picture" +
            " FROM [Receipt] r" +
            " INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id" +
            " INNER JOIN [Product] p ON p.id = rp.idProduct" +
            " INNER JOIN [Materials] m ON m.idProduct = p.id" +
            " WHERE r.username = @username AND r.activeUser = 1 AND m.active = 1 {0}" +
            " ORDER BY dateOfPurchase DESC, receiptNumber DESC, productName ASC";

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access UserForm.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false)
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                if (!userAccess.Equals("B75E7D25-0296-4AD0-833E-DB234C96151E"))
                {
                    M_mainMenu.Items.AddAt(3, new MenuItem("Domov", "homeForm"));
                }
            }

            createCategoryCheckboxes();
        }

        protected void Page_PreRender(object o, EventArgs e)
        {
            // to keep only filtered rows when sorting
            if (ViewState["filterQuery"] != null)
            {
                SDS_materials.SelectCommand = ViewState["filterQuery"].ToString();
                GV_materials.DataBind();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "shopWindow":
                    Server.Transfer("ShopWindow.aspx");
                    break;
                case "homeForm":
                    redirect();
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        private void createCategoryCheckboxes()
        {
            SqlCommand getCategories = new SqlCommand("SELECT id, category FROM [ProductCategory] ORDER BY category ASC", connection);
            connection.Open();
            SqlDataReader reader = getCategories.ExecuteReader();

            while (reader.Read())
            {
                CheckBox cbCategory = new CheckBox();
                cbCategory.ID = "cb_" + reader.GetGuid(0);
                cbCategory.Text = reader.GetString(1) + " |   ";
                cbCategory.CssClass = "checkBoxCategory";
                categoriesHolder.Controls.Add(cbCategory);
            }

            reader.Close();
            connection.Close();
        }

        protected void B_filter_Click(object sender, EventArgs e)
        {
            StringBuilder filterQuery = new StringBuilder();
            bool firstCategoryChecked = false;
            bool closeFilters = false;

            foreach (CheckBox cb in categoriesHolder.Controls)
            {
                if (cb.Checked)
                {
                    closeFilters = true;

                    if (!firstCategoryChecked)
                    {
                        firstCategoryChecked = true;

                        filterQuery.Append(" AND (p.idCategory = '");
                    }
                    else
                    {
                        filterQuery.Append(" OR p.idCategory = '");
                    }

                    filterQuery.Append(cb.ID.Substring(3));
                    filterQuery.Append("'");
                }
            }

            if (closeFilters)
            {
                filterQuery.Append(")");
            }

            SDS_materials.SelectCommand = String.Format(defaultQuery, filterQuery.ToString());
            ViewState["filterQuery"] = String.Format(defaultQuery, filterQuery.ToString());
            GV_materials.DataBind();
        }

        protected void B_removeFilter_Click(object sender, EventArgs e)
        {
            foreach (CheckBox cb in categoriesHolder.Controls)
            {
                cb.Checked = false;
            }

            SDS_materials.SelectCommand = String.Format(defaultQuery, string.Empty);
            ViewState["filterQuery"] = String.Format(defaultQuery, string.Empty);
            GV_materials.DataBind();
        }

        protected void GV_materials_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnPicture = (HiddenField)e.Row.FindControl("hdnPicture");

                if (Convert.ToBoolean(hdnPicture.Value))
                {
                    HiddenField hdnMaterial = (HiddenField)e.Row.FindControl("hdnMaterial");
                    Image imgControl = (Image)e.Row.Cells[5].FindControl("IMG_material");
                    imgControl.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(resizeImage(Convert.FromBase64String(hdnMaterial.Value), 150, 100));
                }
                else
                {
                    Image imgControl = (Image)e.Row.Cells[5].FindControl("IMG_material");
                    imgControl.ImageUrl = "Resources/ic_picture_as_pdf_black_48dp_2x.png";
                }
            }
        }

        protected void B_downloadFile_Click(object sender, EventArgs e)
        {
            Button btnDownload = (Button)sender;
            GridViewRow gvr = (GridViewRow)btnDownload.NamingContainer;
            HiddenField hdnFileName = (HiddenField)gvr.FindControl("hdnFileName");
            HiddenField hdnMaterial = (HiddenField)gvr.FindControl("hdnMaterial");

            Response.AddHeader("Content-disposition", "attachment; filename=" + hdnFileName.Value);
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(Convert.FromBase64String(hdnMaterial.Value));
            Response.End();
        }
    }
}