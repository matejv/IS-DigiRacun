﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="IS_DigiRacun.AddProduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Dodajanje/urejanje artikla - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_materials" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT id, material, description, picture, active, sortOrder, fileName
FROM [Materials]
WHERE idProduct = @idProduct
ORDER BY sortOrder ASC"
                UpdateCommand="UPDATE [Materials]
SET
[description] = @description,
[active] = @active,
[sortOrder] = @sortOrder
WHERE [id] = @id">
                <SelectParameters>
                    <asp:SessionParameter Name="idProduct" SessionField="productId" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="description" />
                    <asp:Parameter Name="active" />
                    <asp:Parameter Name="sortOrder" />
                    <asp:Parameter Name="id" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_productCategory" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT id, category FROM [ProductCategory]
ORDER BY category ASC"></asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniška stran" Value="userForm"></asp:MenuItem>
                            <asp:MenuItem Text="Pregled izdanih računov" Value="sellerForm"></asp:MenuItem>
                            <asp:MenuItem Text="Nazaj na pregled izdelkov v ponudbi" Value="sellerAdminForm"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">
                        <asp:Label ID="L_pageTitle" runat="server" Text="Dodaj izdelek"></asp:Label>
                    </p>
                    <p class="contentH2">Trgovec administrator</p>
                </div>
                <div class="content">
                    <asp:Panel ID="P_addProduct" runat="server" DefaultButton="B_addProduct">
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>
                                    Naziv izdelka:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_productName" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_productName" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_addProduct"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    Cena:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_price" runat="server" ErrorMessage="* Obvezen vnos" Font-Size="XX-Small" ForeColor="#FF3300" ControlToValidate="TB_price" ValidationGroup="VG_addProduct"></asp:RequiredFieldValidator>
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_productName" runat="server" ValidationGroup="VG_addProduct"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_price" runat="server" ValidationGroup="VG_addProduct"></asp:TextBox>
                                    <br />
                                    <asp:RegularExpressionValidator ID="REV_price" runat="server" ErrorMessage="* Neveljavna oblika vnosa" ControlToValidate="TB_price" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="^\d+(\,\d+)*$" ValidationGroup="VG_addProduct"></asp:RegularExpressionValidator>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>
                                    Kategorija izdelka:
                                </p>
                                <p>
                                    V ponudbi:
                                    <br />
                                    <br />
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:DropDownList ID="DDL_productCategory" runat="server" DataSourceID="SDS_productCategory" DataTextField="category" DataValueField="id" ValidationGroup="VG_addProduct">
                                    </asp:DropDownList>
                                </p>
                                <p>
                                    <asp:CheckBox ID="CB_active" runat="server" Checked="True" ValidationGroup="VG_addProduct" />
                                    <br />
                                    <br />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="contentCentral">
                            <p>
                                Opis:
                                <br />
                                <textarea id="TA_description" name="TA_description" maxlength="1000" runat="server"></textarea>
                            </p>
                            <p>
                                Tehnične podrobnosti:
                                <br />
                                <textarea id="TA_techDetails" name="TA_techDetails" maxlength="1000" runat="server"></textarea>
                            </p>
                            <p style="text-align: right; margin-right: 40px;">
                                <asp:Button ID="B_save" runat="server" Text="Shrani spremembe" OnClick="B_save_Click" ValidationGroup="VG_addProduct" Visible="False" />
                                <asp:Button ID="B_addProduct" runat="server" Text="Dodaj izdelek" ValidationGroup="VG_addProduct" OnClick="B_addProduct_Click" />
                                &nbsp;
                                <asp:Button ID="B_cancel" runat="server" Text="Prekliči" OnClick="B_cancel_Click" />
                            </p>
                        </div>
                    </asp:Panel>
                </div>
                <div class="content">
                    <asp:Panel ID="P_manageMaterials" runat="server" DefaultButton="B_uploadFile">
                        <div class="contentHeader">
                            <p class="contentH1">Upravljanje datotek</p>
                            <p class="contentH2">Trgovec administrator</p>
                        </div>
                        <div class="contentCentral">
                            <p>
                                Dodaj datoteko:
                                &nbsp;
                                <asp:FileUpload ID="FU_uploadFile" runat="server" Width="320px" />
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_uploadFile" runat="server" ErrorMessage="* Obvezno polje" ControlToValidate="FU_uploadFile" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_fileUpload"></asp:RequiredFieldValidator>
                            </p>
                        </div>
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>
                                    Opis:
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_fDescription" runat="server"></asp:TextBox>
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="contentHalf">
                            <div class="contentLeft">
                                <p>
                                    Vrstni red:
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_fSortOrder" runat="server" TextMode="Number" min="0" step="1"></asp:TextBox>
                                </p>
                                <p>
                                    <asp:Button ID="B_uploadFile" runat="server" Text="Naloži" OnClick="B_uploadFile_Click" ValidationGroup="VG_fileUpload" />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                    <asp:GridView ID="GV_materials" runat="server" Width="961px" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SDS_materials" OnRowDataBound="GV_materials_RowDataBound" AllowPaging="True" AllowSorting="True">
                        <Columns>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:Button ID="B_update" runat="server" CommandName="Update" Text="OK" />
                                    <asp:Button ID="B_cancel" runat="server" CommandName="Cancel" Text="Prekliči" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="B_edit" runat="server" CommandName="Edit" Text="Uredi" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                            <asp:BoundField DataField="fileName" HeaderText="Ime datoteke" ReadOnly="True" SortExpression="fileName" />
                            <asp:BoundField DataField="material" HeaderText="material" SortExpression="material" Visible="False" />
                            <asp:TemplateField HeaderText="Gradivo">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnMaterial" runat="server" Value='<%# Bind("material") %>' />
                                    <asp:HiddenField ID="hdnPicture" runat="server" Value='<%# Bind("picture") %>' />
                                    <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Bind("fileName") %>' />
                                    <asp:Image ID="IMG_material" runat="server" />
                                    <br />
                                    <asp:Button ID="B_downloadFile" runat="server" OnClick="B_downloadFile_Click" Text="Prenesi" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="description" HeaderText="Opis" SortExpression="description" />
                            <asp:BoundField DataField="sortOrder" HeaderText="sortOrder" SortExpression="sortOrder" Visible="False" />
                            <asp:TemplateField HeaderText="Vrstni red prikaza" SortExpression="sortOrder">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TB_sortOrder" runat="server" Text='<%# Bind("sortOrder") %>' TextMode="Number" min="0" step="1"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="L_sortOrder" runat="server" Text='<%# Bind("sortOrder") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CheckBoxField DataField="picture" HeaderText="Slika" SortExpression="picture" ReadOnly="True" />
                            <asp:CheckBoxField DataField="active" HeaderText="Aktivno" SortExpression="active" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
