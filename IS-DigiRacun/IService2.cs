﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace IS_DigiRacun
{
    [ServiceContract]
    public interface IService2
    {
        // WEBJOB
        [OperationContract]
        [WebInvoke(UriTemplate = "guaranteeExpirationEmailReceipt", ResponseFormat = WebMessageFormat.Json)]
        void guaranteeExpirationEmailReceipt();

        [OperationContract]
        [WebInvoke(UriTemplate = "guaranteeExpirationEmailProduct", ResponseFormat = WebMessageFormat.Json)]
        void guaranteeExpirationEmailProduct();
    }

    [DataContract]
    public class GuaranteeExpirationItem
    {
        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public string productSerialNo { get; set; }

        [DataMember]
        public string dateOfGuarantee { get; set; }

        [DataMember]
        public string receiptNumber { get; set; }

        [DataMember]
        public string shop { get; set; }

        [DataMember]
        public string username { get; set; }

        [DataMember]
        public string email { get; set; }
    }
}
