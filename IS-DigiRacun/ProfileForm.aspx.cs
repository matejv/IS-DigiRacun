﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class ProfileForm : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access ProfileForm.aspx is redirected to Login.aspx
            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false)
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();
                TB_username.Text = Session["user"].ToString();

                if (!Session["access"].ToString().ToUpper().Equals("B75E7D25-0296-4AD0-833E-DB234C96151E"))
                {
                    M_mainMenu.Items.AddAt(1, new MenuItem("Uporabniška stran", "userForm"));
                }

                SqlCommand getUserData = new SqlCommand("SELECT name, surname, email FROM [User] WHERE username = @username", connection);
                getUserData.Parameters.AddWithValue("@username", Session["user"].ToString());
                connection.Open();
                SqlDataReader reader = getUserData.ExecuteReader();

                if (reader.Read())
                {
                    TB_name.Text = reader.IsDBNull(0) ? string.Empty : reader.GetString(0);
                    TB_surname.Text = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                    TB_email.Text = reader.GetString(2);
                }

                reader.Close();
                connection.Close();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "home":
                    redirect();
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void B_changeProfile_Click(object sender, EventArgs e)
        {
            SqlCommand changeProfile = new SqlCommand("UPDATE [User] SET name = @name, surname = @surname, email = @email WHERE username = @username", connection);
            changeProfile.Parameters.AddWithValue("@email", TB_email.Text);
            changeProfile.Parameters.AddWithValue("@username", Session["user"].ToString());

            if (TB_name.Text.Length > 0)
            {
                changeProfile.Parameters.AddWithValue("@name", TB_name.Text);
            }
            else
            {
                changeProfile.Parameters.AddWithValue("@name", DBNull.Value);
            }

            if (TB_surname.Text.Length > 0)
            {
                changeProfile.Parameters.AddWithValue("@surname", TB_surname.Text);
            }
            else
            {
                changeProfile.Parameters.AddWithValue("@surname", DBNull.Value);
            }

            connection.Open();
            changeProfile.ExecuteNonQuery();
            connection.Close();

            Response.Write("<script>alert('Uporabniški profil je bil uspešno spremenjen.');</script>");
        }

        protected void B_changePassword_Click(object sender, EventArgs e)
        {
            if (!checkLogin(Session["user"].ToString(), TB_passwordNow.Text))
            {
                CV_passwordNow.IsValid = false;
                return;
            }

            if (!TB_passwordNew1.Text.Equals(TB_passwordNew2.Text))
            {
                CV_passwordNew2.IsValid = false;
                return;
            }

            SqlCommand changePassword = new SqlCommand("UPDATE [User] SET password = @password WHERE username = @username", connection);
            changePassword.Parameters.AddWithValue("@password", passwordExtract(TB_passwordNew1.Text));
            changePassword.Parameters.AddWithValue("@username", Session["user"].ToString());

            connection.Open();
            changePassword.ExecuteNonQuery();
            connection.Close();

            Response.Write("<script>alert('Geslo je bilo uspešno spremenjeno.');</script>");
        }
    }
}