﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProfileForm.aspx.cs" Inherits="IS_DigiRacun.ProfileForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Uporabniški profil - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Domov" Value="home"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="content">
                    <div class="contentHalf">
                        <asp:Panel ID="P_changeProfile" runat="server" DefaultButton="B_changeProfile">
                            <div class="contentHeader">
                                <p class="contentH1">Urejanje uporabniškega profila</p>
                                <p class="contentH2">Uporabnik</p>
                            </div>
                            <div class="contentLeft">
                                <p>
                                    Uporabniško ime:
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    Ime:
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    Priimek:
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    E-pošta:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_email" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_email" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_changeProfile"></asp:RequiredFieldValidator>
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_username" runat="server" Enabled="False" ValidationGroup="VG_changeProfile"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_name" runat="server" ValidationGroup="VG_changeProfile"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_surname" runat="server" ValidationGroup="VG_changeProfile"></asp:TextBox>
                                    <br />
                                    <br />
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_email" runat="server" TextMode="Email" ValidationGroup="VG_changeProfile"></asp:TextBox>
                                    <br />
                                    <asp:RegularExpressionValidator ID="REV_email" runat="server" ErrorMessage="* Neveljaven e-poštni naslov" Font-Size="XX-Small" ControlToValidate="TB_email" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_changeProfile"></asp:RegularExpressionValidator>
                                </p>
                                <p>
                                    <asp:Button ID="B_changeProfile" runat="server" Text="Shrani spremembe" ValidationGroup="VG_changeProfile" OnClick="B_changeProfile_Click" />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </asp:Panel>
                    </div>
                    <div class="vLine"></div>
                    <div class="contentHalf">
                        <asp:Panel ID="P_changePassword" runat="server" DefaultButton="B_changePassword">
                            <div class="contentHeader">
                                <p class="contentH1">Sprememba gesla</p>
                                <p class="contentH2">Uporabnik</p>
                            </div>
                            <div class="contentLeft">
                                <p>
                                    Trenutno geslo:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_passwordNow" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_passwordNow" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_changePassword"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    Novo geslo:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_passwordNew1" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_passwordNew1" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_changePassword"></asp:RequiredFieldValidator>
                                </p>
                                <p>
                                    Ponovi novo geslo:
                                    <br />
                                    <asp:RequiredFieldValidator ID="RFV_passwordNew2" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_passwordNew2" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_changePassword"></asp:RequiredFieldValidator>
                                </p>
                            </div>
                            <div class="contentRight">
                                <p>
                                    <asp:TextBox ID="TB_passwordNow" runat="server" TextMode="Password" ValidationGroup="VG_changePassword"></asp:TextBox>
                                    <br />
                                    <asp:CustomValidator ID="CV_passwordNow" runat="server" ErrorMessage="* Nepravilno geslo" ControlToValidate="TB_passwordNow" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_changePassword"></asp:CustomValidator>
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_passwordNew1" runat="server" TextMode="Password" ValidationGroup="VG_changePassword"></asp:TextBox>
                                    <br />
                                    <asp:RegularExpressionValidator ID="REV_passwordNew1" runat="server" ErrorMessage="* Geslo mora vsebovati vsaj 8 znakov" ControlToValidate="TB_passwordNew1" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_changePassword" ValidationExpression="^.{8,}$"></asp:RegularExpressionValidator>
                                </p>
                                <p>
                                    <asp:TextBox ID="TB_passwordNew2" runat="server" TextMode="Password" ValidationGroup="VG_changePassword"></asp:TextBox>
                                    <br />
                                    <asp:CustomValidator ID="CV_passwordNew2" runat="server" ErrorMessage="* Gesli se ne ujemata" ControlToValidate="TB_passwordNew2" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_changePassword"></asp:CustomValidator>
                                </p>
                                <p>
                                    <asp:Button ID="B_changePassword" runat="server" Text="Spremeni geslo" ValidationGroup="VG_changePassword" OnClick="B_changePassword_Click" />
                                </p>
                            </div>
                            <div class="clear"></div>
                        </asp:Panel>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
