﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class UserForm : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access UserForm.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false)
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                if (!userAccess.Equals("B75E7D25-0296-4AD0-833E-DB234C96151E"))
                {
                    M_mainMenu.Items.AddAt(3, new MenuItem("Domov", "homeForm"));
                }

                updateGV_receiptsPic();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "shopWindow":
                    Server.Transfer("ShopWindow.aspx");
                    break;
                case "homeForm":
                    redirect();
                    break;
                case "userMaterials":
                    Server.Transfer("UserMaterials.aspx");
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        private void updateGV_receiptsPic()
        {
            GV_receipts.DataBind();
            TB_shop.Text = String.Empty;
            TB_receiptNumber.Text = String.Empty;
            TB_dateOfPurchase.Text = DateTime.Today.ToString("yyyy-MM-dd");
            TB_dateOfGuarantee.Text = String.Empty;
        }

        private void updateGV_receiptsCode()
        {
            GV_receipts.DataBind();
            TB_receiptCode.Text = String.Empty;
        }

        protected void B_addByPicture_Click(object sender, EventArgs e)
        {
            byte[] file = FU_receiptPicture.FileBytes;
            string fileExtension = Path.GetExtension(FU_receiptPicture.FileName);

            SqlCommand newReceiptPicture = new SqlCommand("INSERT INTO [Receipt](id, username, shop, receiptNumber, dateOfPurchase, dateOfGuarantee, receiptPicture, receiptPicFileName, activeUser, activeShop) VALUES (NEWID(), @username, @shop, @receiptNumber, @dateOfPurchase, @dateOfGuarantee, @receiptPicture, @receiptPicFileName, 1, 0)", connection);
            newReceiptPicture.Parameters.AddWithValue("@username", Session["user"].ToString());
            newReceiptPicture.Parameters.AddWithValue("@shop", TB_shop.Text);
            newReceiptPicture.Parameters.AddWithValue("@dateOfPurchase", TB_dateOfPurchase.Text);
            newReceiptPicture.Parameters.AddWithValue("@receiptPicFileName", FU_receiptPicture.FileName);

            if (TB_receiptNumber.Text.Length > 0)
            {
                newReceiptPicture.Parameters.AddWithValue("@receiptNumber", TB_receiptNumber.Text);
            }
            else
            {
                newReceiptPicture.Parameters.AddWithValue("@receiptNumber", DBNull.Value);
            }

            if (DateTime.TryParse(TB_dateOfGuarantee.Text, out DateTime result))
            {
                newReceiptPicture.Parameters.AddWithValue("@dateOfGuarantee", result);
            }
            else
            {
                newReceiptPicture.Parameters.AddWithValue("@dateOfGuarantee", DBNull.Value);
            }

            if (fileExtension.ToLower().Equals(".jpg") || fileExtension.ToLower().Equals(".jpeg") || fileExtension.ToLower().Equals(".png"))
            {
                newReceiptPicture.Parameters.AddWithValue("@receiptPicture", Convert.ToBase64String(resizeImage(file, 1000, 1000)));
            }
            else
            {
                Response.Write("<script>alert('Napaka: Nepodprt format datoteke. Podprti formati so: JPG, JPEG in PNG.');</script>");
                return;
            }

            connection.Open();
            newReceiptPicture.ExecuteNonQuery();
            connection.Close();

            updateGV_receiptsPic();
        }

        protected void B_addByCode_Click(object sender, EventArgs e)
        {
            string receiptId = string.Empty;

            SqlCommand checkCode = new SqlCommand("SELECT id FROM [Receipt] WHERE code = @code AND username IS NULL AND activeUser = 1 AND activeShop = 1", connection);
            checkCode.Parameters.AddWithValue("@code", TB_receiptCode.Text);

            connection.Open();
            SqlDataReader reader = checkCode.ExecuteReader();

            if (reader.Read())
            {
                receiptId = reader.IsDBNull(0) ? string.Empty : reader.GetGuid(0).ToString();
            }

            reader.Close();
            connection.Close();

            if (receiptId.Length == 0)
            {
                Response.Write("<script>alert('Napaka: Neveljavna koda računa.');</script>");
                return;
            }

            SqlCommand newReceiptCode = new SqlCommand("UPDATE [Receipt] SET username = @username WHERE id = @id", connection);
            newReceiptCode.Parameters.AddWithValue("@username", Session["user"].ToString());
            newReceiptCode.Parameters.AddWithValue("@id", receiptId);

            connection.Open();
            newReceiptCode.ExecuteNonQuery();
            connection.Close();

            updateGV_receiptsCode();
        }

        protected void B_edit_Click(object sender, EventArgs e)
        {
            Button btnEdit = (Button)sender;
            GridViewRow gvr = (GridViewRow)btnEdit.NamingContainer;
            Session["receiptId"] = GV_receipts.DataKeys[gvr.RowIndex].Value.ToString();

            Server.Transfer("EditReceipt.aspx");
        }
    }
}