﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="IS_DigiRacun.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Prijava - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small" Visible="False"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Pregled izdelkov za nakup" Value="shopWindow"></asp:MenuItem>
                            <asp:MenuItem Text="Ponastavi geslo" Value="resetPassword"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHalf">
                    <asp:Panel ID="P_registration" runat="server" DefaultButton="B_register">
                        <div class="contentHeader">
                            <p class="contentH1">Registracija</p>
                            <p class="contentH2">Novi uporabniki</p>
                        </div>
                        <div class="contentLeft">
                            <p>
                                Uporabniško ime:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_Rusername" runat="server" ControlToValidate="TB_Rusername" ErrorMessage="* Obvezen vnos" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_Registration"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                Ime:
                                <br />
                                <br />
                            </p>
                            <p>
                                Priimek:
                                <br />
                                <br />
                            </p>
                            <p>
                                E-pošta:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_Remail" runat="server" ErrorMessage="* Obvezen vnos" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_Registration" ControlToValidate="TB_Remail"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                Geslo:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_Rpassword" runat="server" ErrorMessage="* Obvezen vnos" Font-Size="XX-Small" ForeColor="#FF3300" ControlToValidate="TB_Rpassword" ValidationGroup="VG_Registration"></asp:RequiredFieldValidator>
                            </p>
                        </div>
                        <div class="contentRight">
                            <p>
                                <asp:TextBox ID="TB_Rusername" runat="server" ValidationGroup="VG_Registration"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_Rusername" runat="server" ErrorMessage="* Neveljavno ali zasedeno uporabniško ime" Font-Size="XX-Small" ForeColor="#FF3300" ControlToValidate="TB_Rusername" ValidationExpression="^[A-Za-z0-9]{5,20}$" ValidationGroup="VG_Registration"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:TextBox ID="TB_Rname" runat="server" ValidationGroup="VG_Registration"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:TextBox ID="TB_Rsurname" runat="server" ValidationGroup="VG_Registration"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:TextBox ID="TB_Remail" runat="server" TextMode="Email" ValidationGroup="VG_Registration"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_Remail" runat="server" ControlToValidate="TB_Remail" ErrorMessage="* Neveljaven e-poštni naslov" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_Registration"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:TextBox ID="TB_Rpassword" runat="server" TextMode="Password" ValidationGroup="VG_Registration"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_Rpassword" runat="server" ErrorMessage="* Geslo mora vsebovati vsaj 8 znakov" Font-Size="XX-Small" ForeColor="#FF3300" ControlToValidate="TB_Rpassword" ValidationExpression="^.{8,}$" ValidationGroup="VG_Registration"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:Button ID="B_register" runat="server" Text="Registracija" OnClick="B_register_Click" ValidationGroup="VG_Registration" />
                            </p>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                </div>
                <div class="vLine"></div>
                <div class="contentHalf">
                    <asp:Panel ID="P_login" runat="server" DefaultButton="B_login">
                        <div class="contentHeader">
                            <p class="contentH1">Prijava</p>
                            <p class="contentH2">Obstoječi uporabniki</p>
                        </div>
                        <div class="contentLeft">
                            <p>
                                Uporabniško ime:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_Lusername" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_Lusername" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_Login"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                Geslo:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_Lpassword" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_Lpassword" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_Login"></asp:RequiredFieldValidator>
                            </p>
                        </div>
                        <div class="contentRight">
                            <p>
                                <asp:TextBox ID="TB_Lusername" runat="server" ValidationGroup="VG_Login"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:TextBox ID="TB_Lpassword" runat="server" TextMode="Password" ValidationGroup="VG_Login"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:Button ID="B_login" runat="server" Text="Prijava" OnClick="B_login_Click" ValidationGroup="VG_Login" />
                            </p>
                            <p>
                                <br />
                                <asp:HyperLink ID="HL_resetPassword" runat="server" Font-Size="X-Small" NavigateUrl="~/ResetPassword.aspx">Pozabljeno geslo</asp:HyperLink>
                            </p>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </form>
</body>
</html>
