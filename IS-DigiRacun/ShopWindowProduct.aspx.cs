﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace IS_DigiRacun
{
    public partial class ShopWindowProduct : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);
        string idProduct;
        List<ProductPicture> productPictures = new List<ProductPicture>();

        protected void Page_Load(object sender, EventArgs e)
        {
            idProduct = Request.QueryString["id"];

            if (idProduct == null || idProduct.Length == 0)
            {
                Server.Transfer("ShopWindow.aspx");
            }

            if (!IsPostBack)
            {
                if (Session["userLogged"] != null && (bool)Session["userLogged"])
                {
                    L_user.Text = Session["user"].ToString();
                    L_user.Visible = true;

                    M_mainMenu.Items[0].Text = "Odjava";
                    M_mainMenu.Items[0].Value = "logout";
                    M_mainMenu.Items.AddAt(1, new MenuItem("Uporabniški profil", "userProfile"));

                    if (!Session["access"].ToString().ToUpper().Equals("B75E7D25-0296-4AD0-833E-DB234C96151E"))
                    {
                        M_mainMenu.Items.AddAt(2, new MenuItem("Uporabniška stran", "userForm"));
                        M_mainMenu.Items.AddAt(3, new MenuItem("Domov", "homeForm"));
                    }
                    else
                    {
                        M_mainMenu.Items.AddAt(2, new MenuItem("Domov", "homeForm"));
                    }
                }

                getPictures();
                IMG_bigImage.Src = productPictures[0].material;
                HL_productLink.Text = HL_productLink.NavigateUrl = String.Format("https://digiracun.azurewebsites.net/ShopWindowProduct.aspx?id={0}", idProduct);
                getProductData();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "login":
                    Response.Redirect("Login.aspx");
                    break;
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
                case "homeForm":
                    redirect();
                    break;
                case "shopWindow":
                    Server.Transfer("ShopWindow.aspx");
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        private void getPictures()
        {
            SqlCommand getPictures = new SqlCommand("SELECT id, fileName, material FROM [Materials] WHERE idProduct = @idProduct AND picture = 1 AND active = 1 ORDER BY sortOrder ASC", connection);
            getPictures.Parameters.AddWithValue("@idProduct", idProduct);
            connection.Open();
            SqlDataReader reader = getPictures.ExecuteReader();

            while (reader.Read())
            {
                productPictures.Add(new ProductPicture
                {
                    id = reader.GetGuid(0).ToString(),
                    fileName = reader.GetString(1),
                    material = reader.GetString(2)
                });
            }

            reader.Close();
            connection.Close();

            R_pictures.DataSource = productPictures;
            R_pictures.DataBind();
        }

        private void getProductData()
        {
            SqlCommand getProductData = new SqlCommand("SELECT productName, description, techDetails, price, pc.category, s.name, s.address, s.webpage, s.phone, s.email FROM [Product] p INNER JOIN [ProductCategory] pc ON pc.id = p.idCategory INNER JOIN [Shop] s ON s.id = p.idShop WHERE p.id = @idProduct AND p.active = 1", connection);
            getProductData.Parameters.AddWithValue("@idProduct", idProduct);
            connection.Open();
            SqlDataReader reader = getProductData.ExecuteReader();

            if (reader.Read())
            {
                L_productName.Text = reader.GetString(0);
                L_description.Text = reader.IsDBNull(1) ? string.Empty : reader.GetString(1).Replace("\\n", "<br />");
                L_techDetails.Text = reader.IsDBNull(2) ? string.Empty : reader.GetString(2).Replace("\\n", "<br />");
                L_price.Text = String.Format("{0:C}", reader.GetDouble(3));
                L_productCategory.Text = reader.GetString(4);
                L_shop.Text = reader.GetString(5);
                L_address.Text = reader.GetString(6);
                HL_webpage.Text = HL_webpage.NavigateUrl = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
                L_phone.Text = reader.IsDBNull(8) ? string.Empty : reader.GetString(8);
                HL_email.Text = reader.IsDBNull(9) ? string.Empty : reader.GetString(9);
                HL_email.NavigateUrl = reader.IsDBNull(9) ? string.Empty : String.Format("mailto:{0}", reader.GetString(9));
            }

            reader.Close();
            connection.Close();
        }
    }
}