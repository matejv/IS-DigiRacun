﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SellerAdminForm.aspx.cs" Inherits="IS_DigiRacun.SellerAdminForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Trgovec Admin - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_manageProducts" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT p.id, p.productName, p.description, p.price, pc.category, p.active
FROM [Product] p
INNER JOIN [ProductCategory] pc ON pc.id = p.idCategory
WHERE idShop = @idShop">
                <SelectParameters>
                    <asp:SessionParameter Name="idShop" SessionField="shopId" />
                </SelectParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniška stran" Value="userForm"></asp:MenuItem>
                            <asp:MenuItem Text="Pregled izdanih računov" Value="sellerForm"></asp:MenuItem>
                            <asp:MenuItem Text="Dodaj izdelek" Value="addProduct"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="menuBar">
                    <asp:Button ID="B_addProduct" runat="server" Text="DODAJ IZDELEK" Height="60px" Width="120px" OnClick="B_addProduct_Click" />
                </div>
                <div class="contentHeader">
                    <p class="contentH1">Pregled izdelkov v ponudbi</p>
                    <p class="contentH2">Trgovec administrator</p>
                </div>
                <div class="content">
                    <asp:GridView ID="GV_receipts" runat="server" AutoGenerateColumns="False" DataSourceID="SDS_manageProducts" AllowPaging="True" AllowSorting="True" Width="961px" DataKeyNames="id" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="B_editProduct" runat="server" OnClick="B_editProduct_Click" Text="Uredi" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                            <asp:BoundField DataField="productName" HeaderText="Izdelek" SortExpression="productName" />
                            <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" Visible="False" />
                            <asp:TemplateField HeaderText="Opis" SortExpression="description">
                                <ItemTemplate>
                                    <%# ((string)Eval("description")).Length < 140 ? Eval("description") : ((string)Eval("description")).Substring(0,140) + "..." %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="price" HeaderText="Cena" SortExpression="price" DataFormatString="{0:c}" />
                            <asp:BoundField DataField="category" HeaderText="Kategorija" SortExpression="category" />
                            <asp:CheckBoxField DataField="active" HeaderText="V ponudbi" SortExpression="active" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
