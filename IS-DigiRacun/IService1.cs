﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Threading.Tasks;

namespace IS_DigiRacun
{
    [ServiceContract]
    public interface IService1
    {
        // USER
        [OperationContract]
        [WebInvoke(UriTemplate = "Register", ResponseFormat = WebMessageFormat.Json)]
        Task userRegistration(Stream body);

        [OperationContract]
        [WebGet(UriTemplate = "Login", ResponseFormat = WebMessageFormat.Json)]
        bool authenticateUser();

        [OperationContract]
        [WebInvoke(UriTemplate = "EditUser", ResponseFormat = WebMessageFormat.Json)]
        Task editUser(Stream body);

        [OperationContract]
        [WebInvoke(UriTemplate = "ChangeUserPassword", ResponseFormat = WebMessageFormat.Json)]
        Task changeUserPassword(Stream body);

        [OperationContract]
        [WebGet(UriTemplate = "GetUser", ResponseFormat = WebMessageFormat.Json)]
        Task<User> getUser();

        [OperationContract]
        [WebInvoke(UriTemplate = "ResetPassword", ResponseFormat = WebMessageFormat.Json)]
        Task resetPassword(Stream body);

        // RECEIPT
        [OperationContract]
        [WebInvoke(UriTemplate = "AddReceiptPicture", ResponseFormat = WebMessageFormat.Json)]
        Task addReceiptPicture(Stream body);

        [OperationContract]
        [WebInvoke(UriTemplate = "AddReceiptCode", ResponseFormat = WebMessageFormat.Json)]
        Task addReceiptCode(Stream body);

        [OperationContract]
        [WebGet(UriTemplate = "GetReceipts", ResponseFormat = WebMessageFormat.Json)]
        Task<List<Receipt>> getReceipts();

        [OperationContract]
        [WebGet(UriTemplate = "GetReceipt/{receiptId}", ResponseFormat = WebMessageFormat.Json)]
        Task<Receipt> getReceipt(string receiptId);

        [OperationContract]
        [WebInvoke(UriTemplate = "UpdateReceipt", ResponseFormat = WebMessageFormat.Json)]
        Task updateReceipt(Stream body);

        [OperationContract]
        [WebInvoke(UriTemplate = "DeleteReceipt", ResponseFormat = WebMessageFormat.Json)]
        Task deleteReceipt(Stream body);

        // PRODUCT
        [OperationContract]
        [WebGet(UriTemplate = "GetProductsOnReceipt/{receiptId}", ResponseFormat = WebMessageFormat.Json)]
        Task<List<Product>> getProductsOnReceipt(string receiptId);

        [OperationContract]
        [WebGet(UriTemplate = "GetProductsShopWindow", ResponseFormat = WebMessageFormat.Json)]
        Task<List<Product>> getProductsShopWindow();

        [OperationContract]
        [WebGet(UriTemplate = "GetProductShopWindow/{idProduct}", ResponseFormat = WebMessageFormat.Json)]
        Task<ProductShop> getProductShopWindow(string idProduct);

        [OperationContract]
        [WebGet(UriTemplate = "GetProductCategories", ResponseFormat = WebMessageFormat.Json)]
        Task<List<ProductCategory>> getProductCategories();

        // MATERIALS
        [OperationContract]
        [WebGet(UriTemplate = "GetMaterialsOnReceipt/{receiptId}", ResponseFormat = WebMessageFormat.Json)]
        Task<List<Material>> getMaterialsOnReceipt(string receiptId);

        [OperationContract]
        [WebGet(UriTemplate = "GetUserMaterials/?q={productCategoryIds}", ResponseFormat = WebMessageFormat.Json)]
        Task<List<Material>> getUserMaterials(string productCategoryIds);

        [OperationContract]
        [WebGet(UriTemplate = "GetProductPicturesShopWindow/{idProduct}", ResponseFormat = WebMessageFormat.Json)]
        Task<List<Material>> getProductPicturesShopWindow(string idProduct);
    }


    [DataContract]
    public class User
    {
        [DataMember]
        public string username { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public string email { get; set; }

        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string surname { get; set; }
    }

    [DataContract]
    public class Receipt
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string username { get; set; }

        [DataMember]
        public string shop { get; set; }

        [DataMember]
        public string receiptNumber { get; set; }

        [DataMember]
        public string dateOfPurchase { get; set; }

        [DataMember]
        public string dateOfGuarantee { get; set; }

        [DataMember]
        public string receiptPicture { get; set; }

        [DataMember]
        public string receiptPicFileName { get; set; }

        [DataMember]
        public string code { get; set; }
    }

    [DataContract]
    public class Product
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public double price { get; set; }

        [DataMember]
        public string serialNo { get; set; }

        [DataMember]
        public string dateOfGuarantee { get; set; }

        [DataMember]
        public int quantity { get; set; }

        [DataMember]
        public string shop { get; set; }

        [DataMember]
        public string coverPicture { get; set; }
    }

    [DataContract]
    public class ProductCategory
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string category { get; set; }
    }

    [DataContract]
    public class Material
    {
        [DataMember]
        public string materialId { get; set; }

        [DataMember]
        public string productId { get; set; }

        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public string material { get; set; }

        [DataMember]
        public string fileName { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public bool picture { get; set; }

        [DataMember]
        public string shop { get; set; }

        [DataMember]
        public string receiptNumber { get; set; }

        [DataMember]
        public string dateOfPurchase { get; set; }
    }

    [DataContract]
    public class ProductShop
    {
        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public string techDetails { get; set; }

        [DataMember]
        public double price { get; set; }

        [DataMember]
        public string category { get; set; }

        [DataMember]
        public string shopName { get; set; }

        [DataMember]
        public string shopAddress { get; set; }

        [DataMember]
        public string shopWebpage { get; set; }

        [DataMember]
        public string shopPhone { get; set; }

        [DataMember]
        public string shopEmail { get; set; }
    }
}
