﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserMaterials.aspx.cs" Inherits="IS_DigiRacun.UserMaterials" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Gradiva izdelkov na računih - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_materials" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT DISTINCT r.shop, r.receiptNumber, r.dateOfPurchase, p.productName, m.material, m.fileName, m.description, m.picture
FROM [Receipt] r
INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id
INNER JOIN [Product] p ON p.id = rp.idProduct
INNER JOIN [Materials] m ON m.idProduct = p.id
WHERE r.username = @username AND r.activeUser = 1 AND m.active = 1
ORDER BY dateOfPurchase DESC, receiptNumber DESC, productName ASC">
                <SelectParameters>
                    <asp:SessionParameter Name="username" SessionField="user" />
                </SelectParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Pregled izdelkov za nakup" Value="shopWindow"></asp:MenuItem>
                            <asp:MenuItem Text="Nazaj na upravljanje računov" Value="userForm"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">Filtriranje prikaza</p>
                    <p class="contentH2">Uporabnik</p>
                </div>
                <div class="content">
                    <div id="categoriesHolder" class="contentCentral" runat="server"></div>
                    <div class="contentCentral" style="padding-bottom: 20px;">
                        <asp:Button ID="B_filter" runat="server" Text="Filtriraj" OnClick="B_filter_Click" />
                        &nbsp;
                        <asp:Button ID="B_removeFilter" runat="server" Text="Prikaži vse" OnClick="B_removeFilter_Click" />
                    </div>
                    <asp:GridView ID="GV_materials" runat="server" Width="961px" AutoGenerateColumns="False" DataSourceID="SDS_materials" AllowPaging="True" AllowSorting="True" OnRowDataBound="GV_materials_RowDataBound" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:BoundField DataField="shop" HeaderText="Trgovina" SortExpression="shop" />
                            <asp:BoundField DataField="receiptNumber" HeaderText="Št. računa" SortExpression="receiptNumber" />
                            <asp:BoundField DataField="dateOfPurchase" HeaderText="Datum nakupa" SortExpression="dateOfPurchase" DataFormatString="{0:dd. MM. yyyy}" />
                            <asp:BoundField DataField="productName" HeaderText="Artikel" SortExpression="productName" />
                            <asp:BoundField DataField="material" HeaderText="material" SortExpression="material" Visible="False" />
                            <asp:TemplateField HeaderText="Gradivo">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnMaterial" runat="server" Value='<%# Bind("material") %>' />
                                    <asp:HiddenField ID="hdnPicture" runat="server" Value='<%# Bind("picture") %>' />
                                    <asp:HiddenField ID="hdnFileName" runat="server" Value='<%# Bind("fileName") %>' />
                                    <asp:Image ID="IMG_material" runat="server" />
                                    <br />
                                    <asp:Button ID="B_downloadFile" runat="server" OnClick="B_downloadFile_Click" Text="Prenesi" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="fileName" HeaderText="fileName" SortExpression="fileName" Visible="False" />
                            <asp:BoundField DataField="description" HeaderText="Opis" SortExpression="description" />
                            <asp:CheckBoxField DataField="picture" HeaderText="Slika" SortExpression="picture" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
