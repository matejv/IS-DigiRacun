﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class EditReceipt : System.Web.UI.Page
    {
        private static readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);
        private double priceTotal = 0.0;

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access EditReceipt.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false)
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                if (!userAccess.Equals("B75E7D25-0296-4AD0-833E-DB234C96151E"))
                {
                    M_mainMenu.Items.AddAt(3, new MenuItem("Domov", "homeForm"));
                }

                SqlCommand getReceiptData = new SqlCommand("SELECT shop, receiptNumber, dateOfPurchase, dateOfGuarantee, receiptPicture, receiptPicFileName, code FROM [Receipt] WHERE id = @receiptId", connection);
                getReceiptData.Parameters.AddWithValue("@receiptId", Session["receiptId"].ToString());
                connection.Open();
                SqlDataReader reader = getReceiptData.ExecuteReader();

                if (reader.Read())
                {
                    TB_shop.Text = reader.GetString(0);
                    TB_receiptNumber.Text = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                    TB_dateOfPurchase.Text = reader.GetDateTime(2).ToString("yyyy-MM-dd");
                    TB_dateOfGuarantee.Text = reader.IsDBNull(3) ? string.Empty : reader.GetDateTime(3).ToString("yyyy-MM-dd");

                    if (!reader.IsDBNull(4))
                    {
                        ViewState["receiptPicture"] = reader.GetString(4);
                        IMG_receiptPicture.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(resizeImage(Convert.FromBase64String(ViewState["receiptPicture"].ToString()), 700, 700));
                    }
                    else
                    {
                        IMG_receiptPicture.Visible = false;
                        B_downloadPicture.Visible = false;
                    }

                    if (!reader.IsDBNull(5))
                    {
                        ViewState["receiptPicFileName"] = reader.GetString(5);
                    }

                    if (!reader.IsDBNull(6))
                    {
                        TB_shop.Enabled = false;
                        TB_receiptNumber.Enabled = false;
                        TB_dateOfPurchase.Enabled = false;
                        TB_dateOfGuarantee.Enabled = false;
                        L_dateOfGuarantee.Visible = false;
                        TB_dateOfGuarantee.Visible = false;
                        B_save.Visible = false;
                    }
                }

                reader.Close();
                connection.Close();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "shopWindow":
                    Server.Transfer("ShopWindow.aspx");
                    break;
                case "homeForm":
                    redirect();
                    break;
                case "userMaterials":
                    Server.Transfer("UserMaterials.aspx");
                    break;
                case "userForm":
                    Session["receiptId"] = null;
                    Server.Transfer("UserForm.aspx");
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "4FB46B10-557F-4AB4-B70D-1F8516859501": Response.Redirect("AdminForm.aspx"); break;
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                    case "B75E7D25-0296-4AD0-833E-DB234C96151E": Response.Redirect("UserForm.aspx"); break;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void B_downloadPicture_Click(object sender, EventArgs e)
        {
            Response.AddHeader("Content-disposition", "attachment; filename=" + ViewState["receiptPicFileName"]);
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(Convert.FromBase64String(ViewState["receiptPicture"].ToString()));
            Response.End();
        }

        protected void GV_products_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // check row type
            if (e.Row.RowType == DataControlRowType.DataRow)
                // if row type is DataRow, add sumPrice value to totalPrice
                priceTotal += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "sumPrice"));
            else if (e.Row.RowType == DataControlRowType.Footer)
                // if row type is footer, show calculated total value
                e.Row.Cells[6].Text = String.Format("{0:c}", priceTotal);
        }

        protected void B_deleteReceipt_Click(object sender, EventArgs e)
        {
            SqlCommand deleteReceipt = new SqlCommand("UPDATE [Receipt] SET activeUser = 0 WHERE id = @id", connection);
            deleteReceipt.Parameters.AddWithValue("@id", Session["receiptId"].ToString());

            connection.Open();
            deleteReceipt.ExecuteNonQuery();
            connection.Close();

            Server.Transfer("UserForm.aspx");
        }

        protected void B_save_Click(object sender, EventArgs e)
        {
            SqlCommand updateReceipt = new SqlCommand("UPDATE [Receipt] SET shop = @shop, receiptNumber = @receiptNumber, dateOfPurchase = @dateOfPurchase, dateOfGuarantee = @dateOfGuarantee WHERE id = @id AND code IS NULL", connection);
            updateReceipt.Parameters.AddWithValue("@shop", TB_shop.Text);
            updateReceipt.Parameters.AddWithValue("@dateOfPurchase", TB_dateOfPurchase.Text);
            updateReceipt.Parameters.AddWithValue("@id", Session["receiptId"].ToString());

            if (TB_receiptNumber.Text.Length > 0)
            {
                updateReceipt.Parameters.AddWithValue("@receiptNumber", TB_receiptNumber.Text);
            }
            else
            {
                updateReceipt.Parameters.AddWithValue("@receiptNumber", DBNull.Value);
            }

            if (DateTime.TryParse(TB_dateOfGuarantee.Text, out DateTime result))
            {
                updateReceipt.Parameters.AddWithValue("@dateOfGuarantee", result);
            }
            else
            {
                updateReceipt.Parameters.AddWithValue("@dateOfGuarantee", DBNull.Value);
            }

            connection.Open();
            updateReceipt.ExecuteNonQuery();
            connection.Close();
        }

        protected void B_cancel_Click(object sender, EventArgs e)
        {
            Server.Transfer("UserForm.aspx");
        }

        protected void GV_materials_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnPicture = (HiddenField)e.Row.FindControl("hdnPicture");

                if (Convert.ToBoolean(hdnPicture.Value))
                {
                    HiddenField hdnMaterial = (HiddenField)e.Row.FindControl("hdnMaterial");
                    Image imgControl = (Image)e.Row.Cells[2].FindControl("IMG_material");
                    imgControl.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(resizeImage(Convert.FromBase64String(hdnMaterial.Value), 150, 100));
                }
                else
                {
                    Image imgControl = (Image)e.Row.Cells[2].FindControl("IMG_material");
                    imgControl.ImageUrl = "Resources/ic_picture_as_pdf_black_48dp_2x.png";
                }
            }
        }

        protected void B_downloadFile_Click(object sender, EventArgs e)
        {
            Button btnDownload = (Button)sender;
            GridViewRow gvr = (GridViewRow)btnDownload.NamingContainer;
            HiddenField hdnFileName = (HiddenField)gvr.FindControl("hdnFileName");
            HiddenField hdnMaterial = (HiddenField)gvr.FindControl("hdnMaterial");

            Response.AddHeader("Content-disposition", "attachment; filename=" + hdnFileName.Value);
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(Convert.FromBase64String(hdnMaterial.Value));
            Response.End();
        }
    }
}