﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopWindow.aspx.cs" Inherits="IS_DigiRacun.ShopWindow" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Izložba - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_allProducts" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT id, productName, price, name, material FROM
    (SELECT DISTINCT p.id, p.productName, p.price, s.name, m.material, ROW_NUMBER() OVER(PARTITION BY p.id ORDER BY m.sortOrder ASC) rn
    FROM [Product] p
    INNER JOIN [Shop] s ON s.id = p.idShop
    INNER JOIN [Materials] m ON m.idProduct = p.id
    WHERE p.active = 1 AND m.active = 1 AND m.picture = 1) x
WHERE rn = 1"></asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small" Visible="False"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Prijava" Value="login"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">Izložba (pregled izdelkov za nakup)</p>
                    <p class="contentH2">Uporabnik</p>
                </div>
                <div class="content" dir="auto">
                    <asp:ListView ID="LV_allProducts" runat="server" DataSourceID="SDS_allProducts" DataKeyNames="id" GroupItemCount="3" OnItemDataBound="LV_allProducts_ItemDataBound">
                        <AlternatingItemTemplate>
                            <td runat="server" class="shopItem">
                                <asp:HiddenField ID="hdnMaterial" runat="server" Value='<%# Bind("material") %>' />
                                <br />
                                <asp:Image ID="IMG_material" runat="server" />
                                <br />
                                <asp:LinkButton ID="LB_item" runat="server" Text='<%# Eval("productName") %>' OnClick="LB_item_Click"></asp:LinkButton>
                                <br />
                                <asp:Label ID="priceLabel" runat="server" Text='<%# Eval("price", "{0:C}") %>' />
                                <br />
                                <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                <br />
                            </td>
                        </AlternatingItemTemplate>
                        <EditItemTemplate>
                            <td runat="server" style="background-color: #E76122;">id:
                                <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>' />
                                <br />
                                productName:
                                <asp:TextBox ID="productNameTextBox" runat="server" Text='<%# Bind("productName") %>' />
                                <br />
                                price:
                                <asp:TextBox ID="priceTextBox" runat="server" Text='<%# Bind("price") %>' />
                                <br />
                                name:
                                <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
                                <br />
                                material:
                                <asp:TextBox ID="materialTextBox" runat="server" Text='<%# Bind("material") %>' />
                                <br />
                                <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                                <br />
                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                                <br />
                            </td>
                        </EditItemTemplate>
                        <EmptyDataTemplate>
                            <table runat="server" style="border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px;">
                                <tr>
                                    <td>Ni izdelkov za prikaz.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <EmptyItemTemplate>
                            <td runat="server" />
                        </EmptyItemTemplate>
                        <GroupTemplate>
                            <tr id="itemPlaceholderContainer" runat="server">
                                <td id="itemPlaceholder" runat="server"></td>
                            </tr>
                        </GroupTemplate>
                        <InsertItemTemplate>
                            <td runat="server">id:
                                <asp:TextBox ID="idTextBox" runat="server" Text='<%# Bind("id") %>' />
                                <br />
                                productName:
                                <asp:TextBox ID="productNameTextBox" runat="server" Text='<%# Bind("productName") %>' />
                                <br />
                                price:
                                <asp:TextBox ID="priceTextBox" runat="server" Text='<%# Bind("price") %>' />
                                <br />
                                name:
                                <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
                                <br />
                                material:
                                <asp:TextBox ID="materialTextBox" runat="server" Text='<%# Bind("material") %>' />
                                <br />
                                <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                                <br />
                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                                <br />
                            </td>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <td runat="server" class="shopItem">
                                <asp:HiddenField ID="hdnMaterial" runat="server" Value='<%# Bind("material") %>' />
                                <br />
                                <asp:Image ID="IMG_material" runat="server" />
                                <br />
                                <asp:LinkButton ID="LB_item" runat="server" Text='<%# Eval("productName") %>' OnClick="LB_item_Click"></asp:LinkButton>
                                <br />
                                <asp:Label ID="priceLabel" runat="server" Text='<%# Eval("price", "{0:C}") %>' />
                                <br />
                                <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                <br />
                            </td>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <table runat="server">
                                <tr runat="server">
                                    <td runat="server">
                                        <table id="groupPlaceholderContainer" runat="server" border="1" style="border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px;">
                                            <tr id="groupPlaceholder" runat="server">
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server">
                                    <td runat="server" style="text-align: center;">
                                        <asp:DataPager ID="DataPager1" runat="server" PageSize="12">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                            </Fields>
                                        </asp:DataPager>
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <SelectedItemTemplate>
                            <td runat="server" style="background-color: #DFDDDA; font-weight: bold; color: #E76122;">id:
                                <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
                                <br />
                                productName:
                                <asp:Label ID="productNameLabel" runat="server" Text='<%# Eval("productName") %>' />
                                <br />
                                price:
                                <asp:Label ID="priceLabel" runat="server" Text='<%# Eval("price") %>' />
                                <br />
                                name:
                                <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                <br />
                                material:
                                <asp:Label ID="materialLabel" runat="server" Text='<%# Eval("material") %>' />
                                <br />
                            </td>
                        </SelectedItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
