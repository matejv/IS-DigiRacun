﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public partial class AdminForm : System.Web.UI.Page
    {
        private readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access AdminForm.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false ||
                !userAccess.Equals("4FB46B10-557F-4AB4-B70D-1F8516859501"))
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                refresh_DDL_AUshop();
                refresh_DDL_RPusername();
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Server.Transfer("UserForm.aspx");
                    break;
            }
        }

        private void refresh_DDL_AUshop()
        {
            DDL_AUshop.Items.Clear();
            DDL_AUshop.DataSource = SDS_shop_notSeller;
            DDL_AUshop.DataTextField = "name";
            DDL_AUshop.DataValueField = "id";
            DDL_AUshop.DataBind();
            DDL_AUshop.Enabled = false;
        }

        private void refresh_DDL_RPusername()
        {
            DDL_RPusername.DataBind();

            DDL_RPusername.Items.Insert(0, new ListItem("Izberi uporabnika", null));
            DDL_RPusername.SelectedIndex = 0;
        }

        private void refresh()
        {
            GV_manageUsers.DataBind();
            TB_AUusername.Text = string.Empty;
            TB_AUname.Text = string.Empty;
            TB_AUsurname.Text = string.Empty;
            TB_AUemail.Text = string.Empty;
            DDL_AUuserType.SelectedValue = "b75e7d25-0296-4ad0-833e-db234c96151e";
            refresh_DDL_AUshop();
            CB_AUactive.Checked = true;
            RFV_AUusername.IsValid = true;
            REV_AUusername.IsValid = true;
            RFV_AUemail.IsValid = true;
            REV_AUemail.IsValid = true;

            refresh_DDL_RPusername();
        }

        protected void B_addUser_Click(object sender, EventArgs e)
        {
            if (!usernameFree(TB_AUusername.Text))
            {
                REV_AUusername.IsValid = false;
                return;
            }

            string password = generateNewPassword();

            SqlCommand addUser = new SqlCommand("INSERT INTO [User](username, password, email, name, surname, access, active, idShop) VALUES(@username, @password, @email, @name, @surname, @access, @active, @idShop)", connection);
            addUser.Parameters.AddWithValue("@username", TB_AUusername.Text);
            addUser.Parameters.AddWithValue("@password", passwordExtract(password));
            addUser.Parameters.AddWithValue("@email", TB_AUemail.Text);
            addUser.Parameters.AddWithValue("@name", TB_AUname.Text);
            addUser.Parameters.AddWithValue("@surname", TB_AUsurname.Text);
            addUser.Parameters.AddWithValue("@access", DDL_AUuserType.SelectedValue);
            addUser.Parameters.AddWithValue("@active", CB_AUactive.Checked);
            addUser.Parameters.AddWithValue("@idShop", DDL_AUshop.SelectedValue);

            connection.Open();
            addUser.ExecuteNonQuery();
            connection.Close();

            bool passwordSent = sendNewPassword(false, TB_AUusername.Text, password);

            if (!passwordSent)
                Response.Write("<script>alert('Napaka: Pošiljanje e-poštnega sporočila z novim geslom ni uspelo. Uporabnik je bil kljub temu dodan.');</script>");

            refresh();
        }

        protected void B_refresh_Click(object sender, EventArgs e)
        {
            refresh();
        }

        protected void B_resetPassword_Click(object sender, EventArgs e)
        {
            if (getEmail(DDL_RPusername.SelectedValue).Length == 0)
            {
                CV_RPusername.IsValid = false;
                return;
            }

            bool successfull = sendNewPassword(true, DDL_RPusername.SelectedValue, generateNewPassword());

            if (!successfull)
            {
                Response.Write("<script>alert('Napaka: Pošiljanje e-poštnega sporočila z novim geslom ni uspelo.');</script>");
            }
            else
            {
                Response.Write("<script>alert('Uporabnik je prejel e-poštno sporočilo z novim geslom.');</script>");
                refresh_DDL_RPusername();
            }
        }

        private void refreshShop()
        {
            GV_manageShops.DataBind();
            TB_ASshop.Text = string.Empty;
            TB_ASaddress.Text = string.Empty;
            TB_ASphone.Text = string.Empty;
            TB_ASemail.Text = string.Empty;
            TB_ASwebpage.Text = string.Empty;
            RFV_ASshop.IsValid = true;
            RFV_ASaddress.IsValid = true;
            REV_ASemail.IsValid = true;
            REV_ASwebpage.IsValid = true;
        }

        protected void B_addShop_Click(object sender, EventArgs e)
        {
            SqlCommand addShop = new SqlCommand("INSERT INTO [Shop](id, name, address, phone, email, webpage) VALUES (NEWID(), @name, @address, @phone, @email, @webpage)", connection);
            addShop.Parameters.AddWithValue("@name", TB_ASshop.Text);
            addShop.Parameters.AddWithValue("@address", TB_ASaddress.Text);

            if (TB_ASphone.Text.Length > 0)
            {
                addShop.Parameters.AddWithValue("@phone", TB_ASphone.Text);
            }
            else
            {
                addShop.Parameters.AddWithValue("@phone", DBNull.Value);
            }

            if (TB_ASemail.Text.Length > 0)
            {
                addShop.Parameters.AddWithValue("@email", TB_ASemail.Text);
            }
            else
            {
                addShop.Parameters.AddWithValue("@email", DBNull.Value);
            }

            if (TB_ASwebpage.Text.Length > 0)
            {
                addShop.Parameters.AddWithValue("@webpage", TB_ASwebpage.Text);
            }
            else
            {
                addShop.Parameters.AddWithValue("@webpage", DBNull.Value);
            }

            connection.Open();
            addShop.ExecuteNonQuery();
            connection.Close();

            refreshShop();
            refresh();
        }

        protected void B_refreshShop_Click(object sender, EventArgs e)
        {
            refreshShop();
        }

        protected void DDL_AUuserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedUserType = DDL_AUuserType.SelectedValue.ToLower();

            DDL_AUshop.Items.Clear();
            DDL_AUshop.DataTextField = "name";
            DDL_AUshop.DataValueField = "id";

            // selected user type is Administrator or Uporabnik
            if (selectedUserType.Equals("4fb46b10-557f-4ab4-b70d-1f8516859501") || selectedUserType.Equals("b75e7d25-0296-4ad0-833e-db234c96151e"))
            {
                DDL_AUshop.DataSource = SDS_shop_notSeller;
                DDL_AUshop.Enabled = false;
            }
            else
            {
                DDL_AUshop.DataSource = SDS_shop_seller;
                DDL_AUshop.Enabled = true;
            }

            try
            {
                DDL_AUshop.DataBind();
            }
            catch (ArgumentOutOfRangeException)
            {
                DDL_AUshop.SelectedIndex = -1;
            }
        }

        protected void DDL_userType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl_userType = (DropDownList)sender;
            string selectedUserType = ddl_userType.SelectedValue.ToLower();

            GridViewRow gvr = (GridViewRow)ddl_userType.NamingContainer;
            DropDownList ddl_shop = (DropDownList)gvr.FindControl("DDL_shop");

            // selected user type is Administrator or Uporabnik
            if (selectedUserType.Equals("4fb46b10-557f-4ab4-b70d-1f8516859501") || selectedUserType.Equals("b75e7d25-0296-4ad0-833e-db234c96151e"))
            {
                ddl_shop.SelectedValue = "6ee88a65-cc22-49cb-983a-8e5629ce76be";
                ddl_shop.Enabled = false;
            }
            else
            {
                ddl_shop.Enabled = true;
            }
        }

        protected void GV_manageUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow gvr = e.Row;

            if (gvr.RowType != DataControlRowType.DataRow)
                return;

            // disables edit button for logged user
            if (gvr.FindControl("L_username") != null)
            {
                string cellValue = ((Label)gvr.FindControl("L_username")).Text;

                if (cellValue.Equals(Session["user"]))
                {
                    gvr.Cells[0].Enabled = false;
                }
            }

            DropDownList ddl_userType = (DropDownList)gvr.FindControl("DDL_userType");
            DropDownList ddl_shop = (DropDownList)gvr.FindControl("DDL_shop");
            string selectedUserType = ddl_userType.SelectedValue.ToLower();

            // selected user type is Administrator or Uporabnik
            if (selectedUserType.Equals("4fb46b10-557f-4ab4-b70d-1f8516859501") || selectedUserType.Equals("b75e7d25-0296-4ad0-833e-db234c96151e"))
            {
                ddl_shop.Enabled = false;
            }
        }

        protected void GV_manageUsers_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow gvr = ((GridView)sender).Rows[e.RowIndex];
            DropDownList ddl_userType = (DropDownList)gvr.FindControl("DDL_userType");
            DropDownList ddl_shop = (DropDownList)gvr.FindControl("DDL_shop");
            CustomValidator cv_shop = (CustomValidator)gvr.FindControl("CV_shop");
            string selectedUserType = ddl_userType.SelectedValue.ToLower();
            string selectedShop = ddl_shop.SelectedValue.ToLower();

            // selected user type is Trgovec Administrator or Trgovec and empty shop value is selected
            if ((selectedUserType.Equals("d9fe6eb3-5060-44f1-bb1f-5959b589fb8c") || selectedUserType.Equals("b1b443e8-0d7b-4ad3-86e7-9688156a067d"))
                && selectedShop.Equals("6ee88a65-cc22-49cb-983a-8e5629ce76be"))
            {
                cv_shop.IsValid = false;
                e.Cancel = true;
            }
        }

        protected void GV_manageUsers_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            refresh_DDL_RPusername();
        }
    }
}