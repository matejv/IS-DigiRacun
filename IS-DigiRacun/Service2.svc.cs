﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.ServiceModel.Web;
using System.Web.Configuration;
using static IS_DigiRacun.BaseClass;

namespace IS_DigiRacun
{
    public class Service2 : IService2
    {
        private readonly SqlConnection connection = new SqlConnection(WebConfigurationManager.ConnectionStrings["connectDB"].ConnectionString);
        private WebOperationContext ctx = WebOperationContext.Current;

        // WEBJOB
        // Sends an email notification 1 month before guarantee on receipt expires
        public void guaranteeExpirationEmailReceipt()
        {
            string authHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];

            if (authHeader == null || !authHeader.Equals("EAD771D65EC0450B99E10F1E286E8F2E"))
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return;
            }

            List<GuaranteeExpirationItem> items = new List<GuaranteeExpirationItem>();
            SqlCommand getGuaranteeExpiration = new SqlCommand("SELECT r.dateOfGuarantee, r.receiptNumber, r.shop, u.username, u.email FROM [Receipt] r INNER JOIN [User] u ON u.username = r.username WHERE r.dateOfGuarantee = CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) AND r.active = 1 AND u.active = 1", connection);

            connection.Open();
            SqlDataReader reader = getGuaranteeExpiration.ExecuteReader();

            while (reader.Read())
            {
                items.Add(new GuaranteeExpirationItem
                {
                    dateOfGuarantee = reader.IsDBNull(0) ? string.Empty : reader.GetDateTime(0).ToString("dd. MM. yyyy"),
                    receiptNumber = reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                    shop = reader.GetString(2),
                    username = reader.GetString(3),
                    email = reader.GetString(4)
                });
            }

            reader.Close();
            connection.Close();

            // send email
            foreach (GuaranteeExpirationItem item in items)
            {
                string mailBody = string.Format(@"<p>Pozdravljeni, {0}!<br />
                                            <br />
                                            Garancija na računu {1} izdanem v {2} poteče čez 1 mesec, in sicer {3}.<br />
                                            <br />
                                            Lep dan vam želimo,<br />
                                            Ekipa Digi Račun</p><br />",
                                            item.username, item.receiptNumber, item.shop, item.dateOfGuarantee);

                sendEmail(item.email, "Obvestilo o poteku garancije", mailBody);
            }
        }

        // Sends an email notification 1 month before guarantee on product expires
        public void guaranteeExpirationEmailProduct()
        {
            string authHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];

            if (authHeader == null || !authHeader.Equals("3897CE504744442EBD0F818059E4CA3B"))
            {
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return;
            }

            List<GuaranteeExpirationItem> items = new List<GuaranteeExpirationItem>();
            SqlCommand getGuaranteeExpiration = new SqlCommand("SELECT p.productName, rp.productSerialNo, rp.dateOfGuarantee, r.receiptNumber, r.shop, u.username, u.email FROM [ReceiptProduct] rp INNER JOIN [Receipt] r ON r.id = rp.idReceipt INNER JOIN [Product] p ON p.id = rp.idProduct INNER JOIN [User] u ON u.username = r.username WHERE rp.dateOfGuarantee = CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) AND r.active = 1 AND u.active = 1", connection);

            connection.Open();
            SqlDataReader reader = getGuaranteeExpiration.ExecuteReader();

            while (reader.Read())
            {
                items.Add(new GuaranteeExpirationItem
                {
                    productName = reader.GetString(0),
                    productSerialNo = reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                    dateOfGuarantee = reader.IsDBNull(2) ? string.Empty : reader.GetDateTime(2).ToString("dd. MM. yyyy"),
                    receiptNumber = reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                    shop = reader.GetString(4),
                    username = reader.GetString(5),
                    email = reader.GetString(6)
                });
            }

            reader.Close();
            connection.Close();

            // send email
            foreach (GuaranteeExpirationItem item in items)
            {
                string mailBody = string.Format(@"<p>Pozdravljeni, {0}!<br />
                                            <br />
                                            Garancija izdelka {1} (serijska št.: {2}), ki ste ga kupili v {3} (št. računa: {4}) poteče čez 1 mesec, in sicer {5}.<br />
                                            <br />
                                            Lep dan vam želimo,<br />
                                            Ekipa Digi Račun</p><br />",
                                            item.username, item.productName, item.productSerialNo, item.shop, item.receiptNumber, item.dateOfGuarantee);

                sendEmail(item.email, "Obvestilo o poteku garancije", mailBody);
            }
        }
    }
}
