﻿using System;
using System.Web.UI.WebControls;

namespace IS_DigiRacun
{
    public partial class NewReceiptCreated : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // when unregistered user attempts to access NewReceiptCreated.aspx is redirected to Login.aspx
            string userAccess = Session["access"] != null ? Session["access"].ToString().ToUpper() : string.Empty;

            if (Session["userLogged"] == null || (bool)Session["userLogged"] == false ||
                !(userAccess.Equals("B1B443E8-0D7B-4AD3-86E7-9688156A067D") || userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C")))
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                L_user.Text = Session["user"].ToString();

                if (userAccess.Equals("D9FE6EB3-5060-44F1-BB1F-5959B589FB8C"))
                {
                    M_mainMenu.Items.AddAt(3, new MenuItem("Pregled izdelkov v ponudbi", "sellerAdminForm"));
                }

                if (Session["receiptCode"] != null)
                {
                    L_receiptCode.Text = Session["receiptCode"].ToString();
                    IMG_qrCode.ImageUrl = String.Format("http://chart.apis.google.com/chart?cht=qr&chs=400x400&chl={0}", Session["receiptCode"]);
                }
                else
                {
                    redirect();
                }
            }
        }

        protected void M_mainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (M_mainMenu.SelectedItem.Value)
            {
                case "logout":
                    Response.Redirect("Signout.aspx");
                    break;
                case "userProfile":
                    Session["receiptCode"] = null;
                    Server.Transfer("ProfileForm.aspx");
                    break;
                case "userForm":
                    Session["receiptCode"] = null;
                    Server.Transfer("UserForm.aspx");
                    break;
                case "sellerForm":
                    Session["receiptCode"] = null;
                    Server.Transfer("SellerForm.aspx");
                    break;
                case "sellerAdminForm":
                    Session["receiptCode"] = null;
                    Server.Transfer("SellerAdminForm.aspx");
                    break;
            }
        }

        private void redirect()
        {
            if (Session["access"] != null)
            {
                switch (Session["access"].ToString().ToUpper())
                {
                    case "D9FE6EB3-5060-44F1-BB1F-5959B589FB8C": Response.Redirect("SellerAdminForm.aspx"); break;
                    case "B1B443E8-0D7B-4AD3-86E7-9688156A067D": Response.Redirect("SellerForm.aspx"); break;
                }
            }
        }

        protected void B_return_Click(object sender, EventArgs e)
        {
            Session["receiptCode"] = null;

            Server.Transfer("SellerForm.aspx");
        }
    }
}