﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReceiptDetails.aspx.cs" Inherits="IS_DigiRacun.ReceiptDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Podrobnosti računa - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_itemsReceipt" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT rp.id, p.productName, rp.priceUponPurchase, rp.quantity, rp.productSerialNo, rp.dateOfGuarantee, rp.quantity * rp.priceUponPurchase AS 'sumPrice' FROM [Receipt] r
INNER JOIN [ReceiptProduct] rp ON rp.idReceipt = r.id
INNER JOIN [Product] p ON p.id = rp.idProduct
WHERE r.id = @receiptId
ORDER BY p.productName ASC">
                <SelectParameters>
                    <asp:SessionParameter Name="receiptId" SessionField="receiptId" />
                </SelectParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniška stran" Value="userForm"></asp:MenuItem>
                            <asp:MenuItem Text="Nazaj na pregled izdanih računov" Value="sellerForm"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">Podrobnosti računa</p>
                    <p class="contentH2">Trgovec</p>
                </div>
                <div class="content">
                    <div class="contentHalf">
                        <div class="contentLeft">
                            <p>Trgovina:</p>
                            <p>Št. računa:</p>
                        </div>
                        <div class="contentRight">
                            <p>
                                <asp:TextBox ID="TB_shopName" runat="server" ReadOnly="True"></asp:TextBox>
                            </p>
                            <p>
                                <asp:TextBox ID="TB_receiptNumber" runat="server" ReadOnly="True"></asp:TextBox>
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="contentHalf">
                        <div class="contentLeft">
                            <p>Prodajalec:</p>
                            <p>Datum:</p>
                        </div>
                        <div class="contentRight">
                            <p>
                                <asp:TextBox ID="TB_seller" runat="server" ReadOnly="True"></asp:TextBox>
                            </p>
                            <p>
                                <asp:TextBox ID="TB_dateOfPurchase" runat="server" TextMode="Date" ReadOnly="True"></asp:TextBox>
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="content">
                    <asp:GridView ID="GV_products" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" Width="961px" DataSourceID="SDS_itemsReceipt" DataKeyNames="id" ShowFooter="True" OnRowDataBound="GV_products_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                            <asp:BoundField DataField="productName" HeaderText="Artikel" SortExpression="productName" FooterText="Skupaj:">
                                <FooterStyle Font-Bold="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="priceUponPurchase" HeaderText="Cena" SortExpression="priceUponPurchase" DataFormatString="{0:c}" />
                            <asp:BoundField DataField="quantity" HeaderText="Količina" SortExpression="quantity" />
                            <asp:BoundField DataField="productSerialNo" HeaderText="Serijska št." SortExpression="productSerialNo" />
                            <asp:BoundField DataField="dateOfGuarantee" HeaderText="Garancija do" SortExpression="dateOfGuarantee" DataFormatString="{0:dd. MM. yyyy}" />
                            <asp:BoundField DataField="sumPrice" HeaderText="Cena skupaj" SortExpression="sumPrice" ReadOnly="True" DataFormatString="{0:c}" />
                        </Columns>
                    </asp:GridView>
                    <br />
                    <div class="contentHeader">
                        <p class="contentH1">Koda računa</p>
                        <p class="contentH2">Trgovec</p>
                    </div>
                    <div class="content">
                        <p class="contentCode">
                            <asp:Image ID="IMG_qrCode" runat="server" />
                            <br />
                            <asp:Label ID="L_receiptCode" runat="server" Text="Label" Font-Bold="True" Font-Size="Medium"></asp:Label>
                        </p>
                        <div class="contentHalf" style="width: 480px">
                            <p style="text-align: left; margin-left: 40px;">
                                <asp:Button ID="B_deleteReceipt" runat="server" Text="Izbriši račun" OnClick="B_deleteReceipt_Click" OnClientClick="return confirm('Ali ste prepričani, da želite izbrisati račun?')" />
                            </p>
                        </div>
                        <div class="contentHalf" style="width: 480px">
                            <p style="text-align: right; margin-right: 40px;">
                                <asp:Button ID="B_return" runat="server" Text="Nazaj" OnClick="B_return_Click" />
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
