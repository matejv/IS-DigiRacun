﻿CREATE TABLE [dbo].[Shop] (
    [id]      UNIQUEIDENTIFIER NOT NULL,
    [name]    NVARCHAR (50)    NOT NULL,
    [address] NVARCHAR (300)   NOT NULL,
    [phone]   VARCHAR (15)     NULL,
    [email]   NVARCHAR (100)   NULL,
    [webpage] NVARCHAR (100)   NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[UserAccess] (
    [id]       UNIQUEIDENTIFIER NOT NULL,
    [userType] NVARCHAR (50)    NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[User] (
    [username] VARCHAR (20)     NOT NULL,
    [password] VARCHAR (MAX)    NOT NULL,
    [email]    VARCHAR (100)    NOT NULL,
    [name]     NVARCHAR (50)    NULL,
    [surname]  NVARCHAR (50)    NULL,
    [access]   UNIQUEIDENTIFIER NOT NULL,
    [active]   BIT              DEFAULT ((1)) NOT NULL,
    [idShop]   UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([username] ASC),
    CONSTRAINT [FK_User_Shop] FOREIGN KEY ([idShop]) REFERENCES [dbo].[Shop] ([id]),
    CONSTRAINT [FK_User_UserAccess] FOREIGN KEY ([access]) REFERENCES [dbo].[UserAccess] ([id])
);

CREATE TABLE [dbo].[ProductCategory] (
    [id]       UNIQUEIDENTIFIER NOT NULL,
    [category] NVARCHAR (50)    NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

CREATE TABLE [dbo].[Product] (
    [id]          UNIQUEIDENTIFIER NOT NULL,
    [productName] NVARCHAR (50)    NOT NULL,
    [description] NVARCHAR (1000)  NULL,
    [techDetails] NVARCHAR (1000)  NULL,
    [price]       FLOAT (53)       NOT NULL,
    [active]      BIT              DEFAULT ((1)) NOT NULL,
    [idShop]      UNIQUEIDENTIFIER NOT NULL,
    [idCategory]  UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Product_ProductCategory] FOREIGN KEY ([idCategory]) REFERENCES [dbo].[ProductCategory] ([id]),
    CONSTRAINT [FK_Product_Shop] FOREIGN KEY ([idShop]) REFERENCES [dbo].[Shop] ([id])
);

CREATE TABLE [dbo].[ItemsReceiptTEMP] (
    [id]              UNIQUEIDENTIFIER NOT NULL,
    [seller]          VARCHAR (20)     NOT NULL,
    [idProduct]       UNIQUEIDENTIFIER NOT NULL,
    [quantity]        INT              NOT NULL,
    [productSerialNo] VARCHAR (50)     NULL,
    [dateOfGuarantee] DATETIME2 (7)    NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_ItemsReceiptTEMP_User] FOREIGN KEY ([seller]) REFERENCES [dbo].[User] ([username]),
    CONSTRAINT [FK_ItemsReceiptTEMP_Product] FOREIGN KEY ([idProduct]) REFERENCES [dbo].[Product] ([id])
);

CREATE TABLE [dbo].[Materials] (
    [id]          UNIQUEIDENTIFIER NOT NULL,
    [idProduct]   UNIQUEIDENTIFIER NOT NULL,
    [material]    VARCHAR (MAX)    NOT NULL,
    [description] NVARCHAR (20)    NULL,
    [picture]     BIT              NOT NULL,
    [active]      BIT              DEFAULT ((1)) NOT NULL,
    [sortOrder]   INT              NULL,
    [fileName]    NVARCHAR (100)   NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Materials_Product] FOREIGN KEY ([idProduct]) REFERENCES [dbo].[Product] ([id])
);

CREATE TABLE [dbo].[Receipt] (
    [id]                 UNIQUEIDENTIFIER NOT NULL,
    [username]           VARCHAR (20)     NULL,
    [shop]               NVARCHAR (50)    NOT NULL,
    [receiptNumber]      VARCHAR (50)     NULL,
    [dateOfPurchase]     DATETIME2 (7)    NOT NULL,
    [dateOfGuarantee]    DATETIME2 (7)    NULL,
    [receiptPicture]     VARCHAR (MAX)    NULL,
    [code]               VARCHAR (20)     NULL,
    [seller]             VARCHAR (20)     NULL,
    [receiptPicFileName] NVARCHAR (100)   NULL,
    [activeUser]         BIT              DEFAULT ((1)) NOT NULL,
    [activeShop]         BIT              DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Receipt_UserSeller] FOREIGN KEY ([seller]) REFERENCES [dbo].[User] ([username]),
    CONSTRAINT [FK_Receipt_User] FOREIGN KEY ([username]) REFERENCES [dbo].[User] ([username])
);

CREATE TABLE [dbo].[ReceiptProduct] (
    [id]                UNIQUEIDENTIFIER NOT NULL,
    [idReceipt]         UNIQUEIDENTIFIER NOT NULL,
    [idProduct]         UNIQUEIDENTIFIER NOT NULL,
    [productSerialNo]   VARCHAR (50)     NULL,
    [priceUponPurchase] FLOAT (53)       NOT NULL,
    [dateOfGuarantee]   DATETIME2 (7)    NULL,
    [quantity]          INT              DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_ReceiptProduct_Receipt] FOREIGN KEY ([idReceipt]) REFERENCES [dbo].[Receipt] ([id]),
    CONSTRAINT [FK_ReceiptProduct_Product] FOREIGN KEY ([idProduct]) REFERENCES [dbo].[Product] ([id])
);
