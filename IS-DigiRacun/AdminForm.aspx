﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminForm.aspx.cs" Inherits="IS_DigiRacun.AdminForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Admin - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_manageUsers" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT u.[username], u.[email], u.[name], u.[surname], ua.[id] AS 'idUserType', ua.[userType], u.[active], s.[id] AS 'idShop', s.[name] AS 'shopName' FROM [User] u
INNER JOIN [UserAccess] ua ON ua.id = u.access
LEFT JOIN [Shop] s ON s.id = u.idShop"
                UpdateCommand="UPDATE [User]
SET
[email] = @email,
[name] = @name,
[surname] = @surname,
[access] = @idUserType,
[active] = @active,
[idShop] = @idShop
WHERE [username] = @username">
                <UpdateParameters>
                    <asp:Parameter Name="email" />
                    <asp:Parameter Name="name" />
                    <asp:Parameter Name="surname" />
                    <asp:Parameter Name="idUserType" />
                    <asp:Parameter Name="active" />
                    <asp:Parameter Name="idShop" />
                    <asp:Parameter Name="username" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_userAccess" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT [id], [userType] FROM [UserAccess]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_shop" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT [id], [name] FROM [Shop] ORDER BY [name]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_shop_seller" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT [id], [name]
FROM [Shop]
WHERE [id] != '6EE88A65-CC22-49CB-983A-8E5629CE76BE'
ORDER BY [name]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_shop_notSeller" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT [id], [name]
FROM [Shop]
WHERE [id] = '6EE88A65-CC22-49CB-983A-8E5629CE76BE'
ORDER BY [name]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SDS_manageShops" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT id, name, address, phone, email, webpage
FROM [Shop]
WHERE id != '6EE88A65-CC22-49CB-983A-8E5629CE76BE' -- empty value for dropdown
ORDER BY name ASC"
                UpdateCommand="UPDATE [Shop]
SET
name = @name,
address = @address,
phone = @phone,
email = @email,
webpage = @webpage
WHERE id = @id">
                <UpdateParameters>
                    <asp:Parameter Name="name" />
                    <asp:Parameter Name="address" />
                    <asp:Parameter Name="phone" />
                    <asp:Parameter Name="email" />
                    <asp:Parameter Name="webpage" />
                    <asp:Parameter Name="id" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniška stran" Value="userForm"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div class="contentHeader">
                <p class="contentH1">Upravljanje uporabnikov</p>
                <p class="contentH2">Administrator</p>
            </div>
            <div class="content">
                <asp:GridView ID="GV_manageUsers" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SDS_manageUsers" Width="961px" OnRowDataBound="GV_manageUsers_RowDataBound" DataKeyNames="username" OnRowUpdating="GV_manageUsers_RowUpdating" OnRowUpdated="GV_manageUsers_RowUpdated" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:Button ID="B_update" runat="server" CommandName="Update" Text="OK" />
                                &nbsp;
                                <asp:Button ID="B_cancel" runat="server" CommandName="Cancel" Text="Prekliči" CausesValidation="False" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Button ID="B_edit" runat="server" CommandName="Edit" Text="Uredi" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Uporabniško ime" SortExpression="username">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_username" runat="server" Text='<%# Bind("username") %>' Enabled="False"></asp:TextBox>
                                <br />
                                <br />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_username" runat="server" Text='<%# Bind("username") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="E-pošta" SortExpression="email">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_email" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_email" runat="server" ErrorMessage="* Obvezen vnos" Font-Size="XX-Small" ForeColor="#FF3300" ControlToValidate="TB_email"></asp:RequiredFieldValidator>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_email" runat="server" ErrorMessage="* Neveljaven e-poštni naslov" ControlToValidate="TB_email" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_email" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ime" SortExpression="name" ControlStyle-CssClass="manageUsersWidth">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_name" runat="server" Text='<%# Bind("name") %>'></asp:TextBox>
                                <br />
                                <br />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_name" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                            </ItemTemplate>
                            <ControlStyle CssClass="manageUsersWidth"></ControlStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priimek" SortExpression="surname" ControlStyle-CssClass="manageUsersWidth">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_surname" runat="server" Text='<%# Bind("surname") %>'></asp:TextBox>
                                <br />
                                <br />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_surname" runat="server" Text='<%# Bind("surname") %>'></asp:Label>
                            </ItemTemplate>
                            <ControlStyle CssClass="manageUsersWidth"></ControlStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vrsta uporabnika" SortExpression="userType">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDL_userType" runat="server" DataSourceID="SDS_userAccess" DataTextField="userType" DataValueField="id" SelectedValue='<%# Bind("idUserType") %>' AutoPostBack="True" OnSelectedIndexChanged="DDL_userType_SelectedIndexChanged"></asp:DropDownList>
                                <br />
                                <br />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="DDL_userType" runat="server" DataSourceID="SDS_userAccess" DataTextField="userType" DataValueField="id" SelectedValue='<%# Bind("idUserType") %>' AutoPostBack="True" OnSelectedIndexChanged="DDL_userType_SelectedIndexChanged" Enabled="False"></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Trgovina" SortExpression="shopName">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDL_shop" runat="server" DataSourceID="SDS_shop" DataTextField="name" DataValueField="id" SelectedValue='<%# Bind("idShop") %>'></asp:DropDownList>
                                <br />
                                <asp:CustomValidator ID="CV_shop" runat="server" ErrorMessage="* Trgovina ni izbrana" ControlToValidate="DDL_shop" Font-Size="XX-Small" ForeColor="#FF3300"></asp:CustomValidator>
                                <br />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="DDL_shop" runat="server" DataSourceID="SDS_shop" DataTextField="name" DataValueField="id" Enabled="False" SelectedValue='<%# Bind("idShop") %>'></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Aktiven" SortExpression="active">
                            <EditItemTemplate>
                                <asp:CheckBox ID="CB_active" runat="server" Checked='<%# Bind("active") %>' />
                                <br />
                                <br />
                                <br />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CB_active" runat="server" Checked='<%# Bind("active") %>' Enabled="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="hLine"></div>
            <div class="content">
                <br />
                <div class="contentHalf">
                    <asp:Panel ID="P_addUser" runat="server" DefaultButton="B_addUser">
                        <div class="contentHeader">
                            <p class="contentH1">Dodaj uporabnika</p>
                            <p class="contentH2">Administrator</p>
                        </div>
                        <div class="contentLeft">
                            <p>
                                Uporabniško ime:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_AUusername" runat="server" ControlToValidate="TB_AUusername" ErrorMessage="* Obvezen vnos" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_AddUser"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                Ime:
                                <br />
                                <br />
                            </p>
                            <p>
                                Priimek:
                                <br />
                                <br />
                            </p>
                            <p>
                                E-pošta:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_AUemail" runat="server" ErrorMessage="* Obvezen vnos" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_AddUser" ControlToValidate="TB_AUemail"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                Vrsta uporabnika:
                                <br />
                                <br />
                            </p>
                            <p>
                                Zaposlen v trgovini:
                                <br />
                                <br />
                            </p>
                            <p>
                                Aktiven:
                                <br />
                                <br />
                            </p>
                        </div>
                        <div class="contentRight">
                            <p>
                                <asp:TextBox ID="TB_AUusername" runat="server" ValidationGroup="VG_AddUser"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_AUusername" runat="server" ErrorMessage="* Neveljavno ali zasedeno uporabniško ime" Font-Size="XX-Small" ForeColor="#FF3300" ControlToValidate="TB_AUusername" ValidationExpression="^[A-Za-z0-9]{5,20}$" ValidationGroup="VG_AddUser"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:TextBox ID="TB_AUname" runat="server" ValidationGroup="VG_AddUser"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:TextBox ID="TB_AUsurname" runat="server" ValidationGroup="VG_AddUser"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:TextBox ID="TB_AUemail" runat="server" ValidationGroup="VG_AddUser"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_AUemail" runat="server" ControlToValidate="TB_AUemail" ErrorMessage="* Neveljaven e-poštni naslov" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_AddUser"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:DropDownList ID="DDL_AUuserType" runat="server" DataSourceID="SDS_userAccess" DataTextField="userType" DataValueField="id" SelectedValue='<%# "b75e7d25-0296-4ad0-833e-db234c96151e" %>' AutoPostBack="True" OnSelectedIndexChanged="DDL_AUuserType_SelectedIndexChanged" ValidationGroup="VG_AddUser"></asp:DropDownList>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:DropDownList ID="DDL_AUshop" runat="server" SelectedValue='<%# "6ee88a65-cc22-49cb-983a-8e5629ce76be" %>' Enabled="False" ValidationGroup="VG_AddUser"></asp:DropDownList>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:CheckBox ID="CB_AUactive" runat="server" Checked="True" ValidationGroup="VG_AddUser" />
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:Button ID="B_addUser" runat="server" Text="Dodaj uporabnika" ValidationGroup="VG_AddUser" OnClick="B_addUser_Click" />
                                &nbsp;
                                <asp:Button ID="B_refresh" runat="server" OnClick="B_refresh_Click" Text="Ponastavi vnose" CausesValidation="False" ValidationGroup="VG_AddUser" />
                            </p>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                </div>
                <div class="vLine" style="height: 560px;"></div>
                <div class="contentHalf">
                    <asp:Panel ID="P_resetPassword" runat="server" DefaultButton="B_resetPassword">
                        <div class="contentHeader">
                            <p class="contentH1">Ponastavi geslo</p>
                            <p class="contentH2">Administrator</p>
                        </div>
                        <div class="contentLeft">
                            <p>Uporabniško ime:</p>
                            <br />
                            <br />
                        </div>
                        <div class="contentRight">
                            <p>
                                <asp:DropDownList ID="DDL_RPusername" runat="server" DataSourceID="SDS_manageUsers" DataTextField="username" DataValueField="username" ValidationGroup="VG_ResetPassword"></asp:DropDownList>
                                <br />
                                <asp:CustomValidator ID="CV_RPusername" runat="server" ErrorMessage="* E-poštni naslov za izbrano uporabniško ime ni najden" ControlToValidate="DDL_RPusername" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_ResetPassword"></asp:CustomValidator>
                            </p>
                            <p>
                                <asp:Button ID="B_resetPassword" runat="server" OnClick="B_resetPassword_Click" Text="Ponastavi geslo" ValidationGroup="VG_ResetPassword" OnClientClick="return confirm('Ali ste prepričani, da želite izbranemu uporabniku ponastaviti geslo?')" />
                            </p>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div class="content">
                <div class="contentHeader">
                    <p class="contentH1">Upravljanje trgovin</p>
                    <p class="contentH2">Administrator</p>
                </div>
                <asp:GridView ID="GV_manageShops" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SDS_manageShops" Width="961px" AllowPaging="True" AllowSorting="True" ShowHeaderWhenEmpty="True">
                    <Columns>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:Button ID="B_update" runat="server" Text="OK" CommandName="Update" />
                                &nbsp;
                                <asp:Button ID="B_cancel" runat="server" Text="Prekliči" CommandName="Cancel" CausesValidation="False" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Button ID="B_edit" runat="server" CommandName="Edit" Text="Uredi" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                        <asp:TemplateField HeaderText="Trgovina" SortExpression="name">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_shop" runat="server" Text='<%# Bind("name") %>'></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_shop" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_shop" Font-Size="XX-Small" ForeColor="#FF3300"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_shop" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Naslov" SortExpression="address">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_address" runat="server" Text='<%# Bind("address") %>'></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_address" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_address" Font-Size="XX-Small" ForeColor="#FF3300"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_address" runat="server" Text='<%# Bind("address") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Telefon" SortExpression="phone">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_phone" runat="server" Text='<%# Bind("phone") %>'></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_phone" runat="server" ErrorMessage="* Neveljavna telefonska številka" ValidationExpression="^\+*\d+$" ControlToValidate="TB_phone" Font-Size="XX-Small" ForeColor="#FF3300"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_phone" runat="server" Text='<%# Bind("phone") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="E-pošta" SortExpression="email">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_email" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_email" runat="server" ErrorMessage="* Neveljaven e-poštni naslov" ControlToValidate="TB_email" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_email" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Spletna stran" SortExpression="webpage">
                            <EditItemTemplate>
                                <asp:TextBox ID="TB_webpage" runat="server" Text='<%# Bind("webpage") %>'></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_webpage" runat="server" ErrorMessage="* Neveljaven spletni naslov" ControlToValidate="TB_webpage" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="L_webpage" runat="server" Text='<%# Bind("webpage") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="content">
                <br />
                <div class="contentHalf">
                    <asp:Panel ID="P_addShop" runat="server" DefaultButton="B_addShop">
                        <div class="contentHeader">
                            <p class="contentH1">Dodaj trgovino</p>
                            <p class="contentH2">Administrator</p>
                        </div>
                        <div class="contentLeft">
                            <p>
                                Ime trgovine:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_ASshop" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_ASshop" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_AddShop"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                Naslov:
                                <br />
                                <asp:RequiredFieldValidator ID="RFV_ASaddress" runat="server" ErrorMessage="* Obvezen vnos" ControlToValidate="TB_ASaddress" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_AddShop"></asp:RequiredFieldValidator>
                            </p>
                            <p>
                                Telefon:
                                <br />
                                <br />
                            </p>
                            <p>
                                E-pošta:
                                <br />
                                <br />
                            </p>
                            <p>
                                Spletna stran:
                                <br />
                                <br />
                            </p>
                        </div>
                        <div class="contentRight">
                            <p>
                                <asp:TextBox ID="TB_ASshop" runat="server" ValidationGroup="VG_AddShop"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:TextBox ID="TB_ASaddress" runat="server" ValidationGroup="VG_AddShop"></asp:TextBox>
                                <br />
                                <br />
                            </p>
                            <p>
                                <asp:TextBox ID="TB_ASphone" runat="server" ValidationGroup="VG_AddShop"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_ASphone" runat="server" ErrorMessage="* Neveljavna telefonska številka" ValidationExpression="^\+*\d+$" ControlToValidate="TB_ASphone" Font-Size="XX-Small" ForeColor="#FF3300" ValidationGroup="VG_AddShop"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:TextBox ID="TB_ASemail" runat="server" ValidationGroup="VG_AddShop"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_ASemail" runat="server" ErrorMessage="* Neveljaven e-poštni naslov" ControlToValidate="TB_ASemail" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VG_AddShop"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:TextBox ID="TB_ASwebpage" runat="server" ValidationGroup="VG_AddShop"></asp:TextBox>
                                <br />
                                <asp:RegularExpressionValidator ID="REV_ASwebpage" runat="server" ErrorMessage="* Neveljaven spletni naslov" ControlToValidate="TB_ASwebpage" Font-Size="XX-Small" ForeColor="#FF3300" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?" ValidationGroup="VG_AddShop"></asp:RegularExpressionValidator>
                            </p>
                            <p>
                                <asp:Button ID="B_addShop" runat="server" Text="Dodaj trgovino" ValidationGroup="VG_AddShop" OnClick="B_addShop_Click" />
                                &nbsp;
                                <asp:Button ID="B_refreshShop" runat="server" Text="Ponastavi vnose" CausesValidation="False" ValidationGroup="VG_AddShop" OnClick="B_refreshShop_Click" />
                            </p>
                        </div>
                        <div class="clear"></div>
                    </asp:Panel>
                </div>
                <div class="contentHalf">
                    <div class="contentLeft"></div>
                    <div class="contentRight"></div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </form>
</body>
</html>
