﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SellerForm.aspx.cs" Inherits="IS_DigiRacun.SellerForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Trgovec - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <asp:SqlDataSource ID="SDS_receipt" runat="server" ConnectionString="<%$ ConnectionStrings:connectDB %>" SelectCommand="SELECT * FROM [Receipt]
WHERE [seller] = @seller AND [activeShop] = 1
ORDER BY [dateOfPurchase] DESC, [receiptNumber] DESC">
                <SelectParameters>
                    <asp:SessionParameter Name="seller" SessionField="user" />
                </SelectParameters>
            </asp:SqlDataSource>
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Odjava" Value="logout"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniški profil" Value="userProfile"></asp:MenuItem>
                            <asp:MenuItem Text="Uporabniška stran" Value="userForm"></asp:MenuItem>
                            <asp:MenuItem Text="Nov račun" Value="newReceipt"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="menuBar">
                    <asp:Button ID="B_newReceipt" runat="server" Text="NOV RAČUN" Height="60px" Width="100px" OnClick="B_newReceipt_Click" />
                </div>
                <div class="contentHeader">
                    <p class="contentH1">Pregled izdanih računov</p>
                    <p class="contentH2">Trgovec</p>
                </div>
                <div class="content">
                    <asp:GridView ID="GV_receipts" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" Width="961px" DataKeyNames="id" DataSourceID="SDS_receipt" ShowHeaderWhenEmpty="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="B_receiptDetails" runat="server" OnClick="B_receiptDetails_Click" Text="Podrobnosti" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" Visible="False" />
                            <asp:BoundField DataField="receiptNumber" HeaderText="Št. računa" SortExpression="receiptNumber" />
                            <asp:BoundField DataField="dateOfPurchase" HeaderText="Datum nakupa" SortExpression="dateOfPurchase" DataFormatString="{0:dd. MM. yyyy}" />
                            <asp:BoundField DataField="code" HeaderText="Koda" SortExpression="code" />
                            <asp:BoundField DataField="seller" HeaderText="Prodajalec" SortExpression="seller" />
                            <asp:BoundField DataField="shop" HeaderText="Trgovina" SortExpression="shop" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
