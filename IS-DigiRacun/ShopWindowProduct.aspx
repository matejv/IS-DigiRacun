﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopWindowProduct.aspx.cs" Inherits="IS_DigiRacun.ShopWindowProduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" href="Resources/DigiRacun-favicon.png" />
    <title>Podrobnosti izdelka - Digi Račun</title>
    <link rel="stylesheet" type="text/css" href="StyleSheet1.css" />
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#ImageGallery img').click(function () {
                var bigImage = $(this).attr('src');
                $('#IMG_bigImage').attr('src', bigImage);
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div>
                <div class="logo">
                    <img src="Resources/DigiRacun-logo.png" alt="Logo" class="logoImg" />
                </div>
                <div class="mainMenu" dir="rtl">
                    <asp:Label ID="L_user" runat="server" Font-Bold="True" Style="padding-right: 20px;" Font-Size="Small" Visible="False"></asp:Label>
                    <br />
                    <asp:Menu ID="M_mainMenu" runat="server" Font-Italic="True" Font-Size="Small" Font-Strikeout="False" OnMenuItemClick="M_mainMenu_MenuItemClick">
                        <Items>
                            <asp:MenuItem Text="Prijava" Value="login"></asp:MenuItem>
                            <asp:MenuItem Text="Nazaj na vse izdelke" Value="shopWindow"></asp:MenuItem>
                        </Items>
                        <StaticHoverStyle BackColor="#DFDDDA" Font-Bold="True" Font-Italic="True" ForeColor="#E76122" />
                        <StaticMenuItemStyle ForeColor="White" />
                        <StaticMenuStyle HorizontalPadding="20px" />
                    </asp:Menu>
                </div>
                <div class="clear"></div>
            </div>
            <div class="hLine"></div>
            <div>
                <div class="contentHeader">
                    <p class="contentH1">
                        <asp:Label ID="L_productName" runat="server"></asp:Label>
                    </p>
                    <p class="contentH2">Izložba</p>
                </div>
                <div class="content">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="150px" valign="top" align="center" style="border-right: 3px solid #EEEEEE">
                                <div id="ImageGallery" style="overflow: auto; height: 350px; width: 130px; display: inline-block;">
                                    <asp:Repeater ID="R_pictures" runat="server">
                                        <ItemTemplate>
                                            <img src='<%#Eval("material") %>' alt='<%#Eval("fileName") %>' width="100px" style="cursor: pointer" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                            <td valign="top" align="center">
                                <img id="IMG_bigImage" alt="" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <div class="contentCentral">
                        <p>
                            <asp:Label ID="L_productCategory" runat="server" Font-Size="Small"></asp:Label>
                        </p>
                        <p style="text-align: justify;">
                            <asp:Label ID="L_description" runat="server"></asp:Label>
                        </p>
                        <p>
                            Cena:
                            &nbsp;
                            <asp:Label ID="L_price" runat="server" Font-Bold="True" Font-Size="Large"></asp:Label>
                        </p>
                        <p>
                            <b>Tehnične podrobnosti:</b>
                            <br />
                            <asp:Label ID="L_techDetails" runat="server"></asp:Label>
                        </p>
                        <p>
                            Povezava:
                            &nbsp;
                            <asp:HyperLink ID="HL_productLink" runat="server" Font-Bold="False">[HL_productLink]</asp:HyperLink>
                        </p>
                    </div>
                    <div class="clear"></div>
                    <div class="hLine"></div>
                    <div class="contentCentral">
                        <div class="contentHeader">
                            <p class="contentH1">Podatki o prodajalcu</p>
                        </div>
                        <div class="contentLeft">
                            <p>
                                Trgovina:
                            </p>
                            <p>
                                Naslov:
                            </p>
                            <p>
                                Spletna stran:
                            </p>
                            <p>
                                Telefon:
                            </p>
                            <p>
                                E-pošta:
                            </p>
                        </div>
                        <div class="contentRight" style="width: 600px;">
                            <p>
                                <asp:Label ID="L_shop" runat="server"></asp:Label>
                            </p>
                            <p>
                                <asp:Label ID="L_address" runat="server"></asp:Label>
                            </p>
                            <p>
                                <asp:HyperLink ID="HL_webpage" runat="server">[HL_webpage]</asp:HyperLink>
                            </p>
                            <p>
                                <asp:Label ID="L_phone" runat="server"></asp:Label>
                            </p>
                            <p>
                                <asp:HyperLink ID="HL_email" runat="server">[HL_email]</asp:HyperLink>
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
